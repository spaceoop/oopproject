# OOP Project #

This is the working repository for OOP 2017-18 Semester 1 Project 3G4.

### How do I get set up? ###

The project work best with the Eclipse environment. To import the project in Eclipse:

1. File -> Import
2. Choose Git -> Projects from Git
3. Choose Clone URI
4. Copy the url from bitbucket page (can be found in Overview), e.g. `https://STommydx@bitbucket.org/spaceoop/oopproject.git`
5. Type in your bitbucket password and click next
6. Choose Import existing Eclipse projects
7. Choose OOPProject

### Contribution guidelines ###

The project would not be successful without your contribution! Follow the steps to create a new feature:

1. Right-click OOPProject, and find the Team option.
2. Team -> Switch to -> New Branch
3. Set source as `master`
4. Name your branch as `feature/yourfeaturename`
5. Make awesome changes to the code!
6. Commit your changes using Team -> Commit, remember to stage your changes and make a nice commit message

Before submitting, you should clean up your work:

1. Switch to `master` branch, do a pull to update latest changes
2. Switch back to `feature/yourfeaturename`, run a rebase with master
3. Resolve conflicts if needed, seek help if you don't know how to do so

Lastly push the branch to bitbucket and create a PR:

1. Team -> Push Branch `feature/yourfeaturename`
2. Go to BitBucket and create a pull request
3. Set the source as `feature/yourfeaturename` and merge into `master`
4. Wait for approval!

If you are a crazy person and want to know more, refer to the following 2 links for more details:

1. [GitHub Standard Fork & Pull Request Workflow](https://gist.github.com/Chaser324/ce0505fbed06b947d962)
2. [Pull Requests | Atlassian Git Tutorial](https://www.atlassian.com/git/tutorials/making-a-pull-request)

### Who do I talk to? ###

If you got any problems, feel free to drop a WhatsApp to the group leader Tommy Li. He will kindly answer you asap. :)