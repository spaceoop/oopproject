rm 03g4_oop.zip
rm 03g4_oop_Doc.zip
cd OOPProject
rm -rf bin
rm 03g4_oop.jar
mkdir bin
javac -sourcepath src -d bin src/hk/hku/learner/oop/OOPProject.java
jar cfe 03g4_oop.jar hk.hku.learner.oop.OOPProject -C bin .
zip -qr ../03g4_oop.zip 03g4_c.bat 03g4_r.bat 03g4_oop.jar ABN_TEST assets bin data N_Test src
zip -qr ../03g4_oop_Doc.zip *.pdf readme_03g4_oop.txt

