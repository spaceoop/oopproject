@echo off

rem Add path for KEC Computer Lab
set PATH=%PATH%;C:\Program Files\Java\jdk1.8.0_141\bin

rem Clean up old files
rmdir /S /Q bin
mkdir bin
echo Previous class files in bin was cleaned up.

rem Compile class files
javac -sourcepath src -d bin src\hk\hku\learner\oop\OOPProject.java
echo Class files compiled in bin directory.

rem Make jar file
jar cfe 03g4_oop.jar hk.hku.learner.oop.OOPProject -C bin .
echo Jar archive file created.

pause