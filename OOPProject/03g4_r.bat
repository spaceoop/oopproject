@echo off
if exist 03g4_oop.jar (
	echo Jar file exists, run using jar file.
	java -jar 03g4_oop.jar
) else (
	echo Jar file does not exist, run using class files in bin.
	java -cp bin hk.hku.learner.oop.OOPProject
)
pause