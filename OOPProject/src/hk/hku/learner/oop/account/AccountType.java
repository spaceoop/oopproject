package hk.hku.learner.oop.account;

public enum AccountType {
	ADM("Administrator"),
	SP("Service Provider"),
	SC("Service Consumer");
	
	private final String typeName;
	
	private AccountType(String name) {
		typeName = name;
	}
	
	public String getTypeName() {
		return typeName;
	}
	
	public static AccountType getType(String type) {
		if(type.equals(ADM.getTypeName()))
			return ADM;
		else if(type.equals(SP.getTypeName()))
			return SP;
		else
			return SC;
	}
}
