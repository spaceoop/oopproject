package hk.hku.learner.oop.account;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public final class PasswordHasher {
	
	private static final SecureRandom RANDOM = new SecureRandom();
	
	private PasswordHasher() {
	}
	
	public static String getHashedPassword(char[] pwdStr, String salt) {
		KeySpec spec = new PBEKeySpec(pwdStr, salt.getBytes(), 65536, 128);
		try {
			SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash = f.generateSecret(spec).getEncoded();
			Base64.Encoder enc = Base64.getEncoder();			
			return enc.encodeToString(hash);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
			System.err.println("ERROR: Error in hashing password");
			return "CANNOTHASHPWD";
		}
	}
	
	public static String getNextSalt() {
		byte[] salt = new byte[16];
		RANDOM.nextBytes(salt);
		Base64.Encoder enc = Base64.getEncoder();
		return enc.encodeToString(salt);
	}
	
	public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeySpecException{
		String pwd = "1wdvzse4";
		String mySalt = PasswordHasher.getNextSalt();
		String myHash = PasswordHasher.getHashedPassword(pwd.toCharArray(), mySalt);
		System.out.println("pwd:" + pwd);
		System.out.println("salt:" + mySalt);
		System.out.println("hash:" + myHash);
	}
	
}
