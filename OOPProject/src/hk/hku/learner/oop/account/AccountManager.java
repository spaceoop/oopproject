package hk.hku.learner.oop.account;

import java.util.ArrayList;

import hk.hku.learner.oop.database.DataManager;
import hk.hku.learner.oop.database.DataRecord;
import hk.hku.learner.oop.database.DataTable;
import hk.hku.learner.oop.database.RecordNotFoundException;
import hk.hku.learner.oop.database.field.BooleanField;
import hk.hku.learner.oop.database.field.DateField;
import hk.hku.learner.oop.database.field.DropdownField;
import hk.hku.learner.oop.database.field.Field;
import hk.hku.learner.oop.database.field.InvalidDataException;
import hk.hku.learner.oop.database.field.StringField;
import hk.hku.learner.oop.gui.filter.BooleanFilterPanel;
import hk.hku.learner.oop.gui.filter.PatternFilterPanel;
import hk.hku.learner.oop.gui.filter.SelectionFilterPanel;

public class AccountManager extends DataManager<Account> {

	private StringField acField, saltField, passwordField, nameField;
	private DropdownField genderField, acTypeField;
	private BooleanField approveField;
	private DateField dobField;

	public AccountManager(Account managerAccount) {
		ArrayList<Field<?>> meta = new ArrayList<Field<?>>();

		acField = new StringField("Username");
		meta.add(acField);
		filterTables.add(new PatternFilterPanel<StringField>(acField));

		saltField = new StringField("Password Salt");
		saltField.setEditable(false);
		saltField.setVisible(false);
		meta.add(saltField);

		passwordField = new StringField("Hashed Password");
		passwordField.setEditable(false);
		passwordField.setVisible(false);
		meta.add(passwordField);

		nameField = new StringField("Name");
		meta.add(nameField);
		filterTables.add(new PatternFilterPanel<StringField>(nameField));

		dobField = new DateField("Date of Birth");
		meta.add(dobField);
		filterTables.add(new PatternFilterPanel<DateField>(dobField));

		String[] sex = { "M", "F" };
		genderField = new DropdownField("Gender", sex);
		meta.add(genderField);
		filterTables.add(new SelectionFilterPanel<DropdownField>(genderField));

		approveField = new BooleanField("Approved");
		filterTables.add(new BooleanFilterPanel(approveField));
		meta.add(approveField);

		String[] acType = { AccountType.ADM.getTypeName(), AccountType.SP.getTypeName(), AccountType.SC.getTypeName() };
		acTypeField = new DropdownField("Account Type", acType);
		meta.add(acTypeField);
		filterTables.add(new SelectionFilterPanel<DropdownField>(acTypeField));

		table = new DataTable(meta);

	}

	public Account findAccount(String username) throws RecordNotFoundException {
		int recID = table.searchRecord(acField, username);
		try {
			return getInstance(table.getRecordString(recID));
		} catch (InvalidDataException e) {
			throw new RecordNotFoundException();
		}
	}

	@Override
	public DataRecord getRecord(Account ac) throws InvalidDataException {
		DataRecord dr = new DataRecord(ac.getID());
		dr.setField(nameField, ac.getName());
		dr.setField(acField, ac.getUsername());
		dr.setField(saltField, ac.getSalt());
		dr.setField(passwordField, ac.getHashedPassword());
		dr.setField(dobField, ac.getDOB());
		dr.setField(genderField, ac.getGender());
		dr.setField(approveField, ac.getApproved());
		dr.setField(acTypeField, ac.getAccountType().getTypeName());
		return dr;
	}

	@Override
	public Account getInstance(String[] str) throws InvalidDataException {
		return new Account(str);
	}

	public static Account getLoginManager() {
		String[] str = new String[9];
		str[0] = "-1";
		str[7] = "true";
		str[8] = AccountType.ADM.getTypeName();
		try {
			return new Account(str);
		} catch (InvalidDataException e) {
			System.err.println(
					"INTERNAL ERROR: This error should not be happening.\nPlease report the issue to the software developer.");
			return null;
		}
	}

}
