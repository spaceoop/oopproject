package hk.hku.learner.oop.account;

import hk.hku.learner.oop.database.Identifiable;
import hk.hku.learner.oop.database.field.InvalidDataException;

public class Account implements Identifiable {
	private int id;
	private String ac, name;
	private AccountType accountType;
	private String hashedPassword;
	private String dob;
	private String gender;
	private boolean approved;
	private String salt;
	
	public Account(String[] args) throws InvalidDataException {
		if (args.length != 9)
			throw new InvalidDataException("String[9]", "String[" + args.length + "]");
		try {
			id = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			throw new InvalidDataException("Integer ID", args[0].equals("") ? "Empty String" : "String " + args[0]);
		}
		ac = args[1];
		salt = args[2];
		hashedPassword = args[3];
		name = args[4];
		dob = args[5];
		gender = args[6];
		approved = Boolean.parseBoolean(args[7]);
		accountType = AccountType.getType(args[8]);
	}
	
	@Override
	public int getID() {
		return id;
	}
	
	public String getUsername() {
		return ac;
	}
	
	public void setUsername(String username) {
		ac = username;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String newName) {
		name = newName;
	}
	
	public AccountType getAccountType() {
		return accountType;
	}

	public String getHashedPassword() {
		return hashedPassword;
	}
	
	public void setHashedPassword(String newHashedPassword) {
		hashedPassword = newHashedPassword;
	}

	public String getDOB() {
		return dob;
	}
	
	public void setDOB(String newDOB) {
		dob = newDOB;
	}

	public String getGender() {
		return gender;
	}
	
	public void setGender(String newGender) {
		gender = newGender;
	}

	public Boolean getApproved() {
		return approved;
	}

	public String getSalt() {
		return salt;
	}
}
