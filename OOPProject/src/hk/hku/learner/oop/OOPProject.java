package hk.hku.learner.oop;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import hk.hku.learner.oop.gui.login.LoginWindow;

public class OOPProject {

	public static void main(String[] args) {
		
		// Set look and feel to windows style
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			System.err.println("ERROR: Cannot set UI to system look & feel, using default L&F instead.");
		}
		
		// Display welcome message
		System.out.println("Welcome to OOP delivery system!");

		// Run GUI
		SwingUtilities.invokeLater(() -> new LoginWindow());
		
	}

}
