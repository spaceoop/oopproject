package hk.hku.learner.oop.food;

import hk.hku.learner.oop.database.Identifiable;
import hk.hku.learner.oop.database.field.InvalidDataException;

public class Order implements Identifiable {
	
	private int id, proviID, cusID, foodID;
	private String fee, reqTime, startTime, finTime;
	private OrderStatus status;
	
	public Order(String[] args) throws InvalidDataException {
		if (args.length != 9)
			throw new InvalidDataException("String[9]", "String[" + args.length + "]");
		try {
			id = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			throw new InvalidDataException("Integer ID", args[0].equals("") ? "Empty String" : "String " + args[0]);
		}
		try {
			proviID = Integer.parseInt(args[1]);
		} catch (NumberFormatException e) {
			throw new InvalidDataException("Integer ID", args[1].equals("") ? "Empty String" : "String " + args[1]);
		}
		try {
			cusID = Integer.parseInt(args[2]);
		} catch (NumberFormatException e) {
			throw new InvalidDataException("Integer ID", args[2].equals("") ? "Empty String" : "String " + args[2]);
		}
		try {
			foodID = Integer.parseInt(args[3]);
		} catch (NumberFormatException e) {
			throw new InvalidDataException("Integer ID", args[3].equals("") ? "Empty String" : "String " + args[3]);
		}
		fee = args[4];
		reqTime = args[5];
		startTime = args[6];
		finTime = args[7];
		status = OrderStatus.getStatus(args[8]);
	}
	
	@Override
	public int getID() {
		return id;
	}
	
	public Integer getProviderID() {
		return proviID;
	}
	
	public Integer getCustomerID() {
		return cusID;
	}
	
	public Integer getFoodID() {
		return foodID;
	}
	
	public String getFee() {
		return fee;
	}
	
	public String getRequestTime() {
		return reqTime;
	}
	
	public String getStartTime() {
		return startTime;
	}
	
	public String getFinishTime() {
		return finTime;
	}
	
	public OrderStatus getOrderStatus() {
		return status;
	}
	
}
