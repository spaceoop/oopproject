package hk.hku.learner.oop.food;

import java.util.ArrayList;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.account.AccountType;
import hk.hku.learner.oop.database.DataManager;
import hk.hku.learner.oop.database.DataRecord;
import hk.hku.learner.oop.database.DataTable;
import hk.hku.learner.oop.database.field.DropdownField;
import hk.hku.learner.oop.database.field.Field;
import hk.hku.learner.oop.database.field.IntegerField;
import hk.hku.learner.oop.database.field.InvalidDataException;
import hk.hku.learner.oop.database.field.PatternField;
import hk.hku.learner.oop.database.field.TimeField;
import hk.hku.learner.oop.gui.filter.CompareFilterPanel;
import hk.hku.learner.oop.gui.filter.SelectionFilterPanel;

public class OrderManager extends DataManager<Order> {
	
	private IntegerField foodField, proviField, custField;
	private TimeField reqField, startField, finField;
	private DropdownField statusField;
	private PatternField feeField;
	
	public OrderManager(Account managerAccount) {
		ArrayList<Field<?>> meta = new ArrayList<Field<?>>();
		
		proviField = new IntegerField("Provider ID", 100000, 199999);
		meta.add(proviField);
		filterTables.add(new CompareFilterPanel<Integer, IntegerField>(proviField));
		
		custField = new IntegerField("Customer ID", 100000, 199999);
		meta.add(custField);
		filterTables.add(new CompareFilterPanel<Integer, IntegerField>(custField));
		
		foodField = new IntegerField("Food ID", 200000, 299999);
		meta.add(foodField);
		filterTables.add(new CompareFilterPanel<Integer, IntegerField>(foodField));
		
		feeField = new PatternField("Fee (HKD)", "[0-9]*.[0-9]{2}");
		meta.add(feeField);
		filterTables.add(new CompareFilterPanel<String, PatternField>(feeField));
		 		
		reqField = new TimeField("Request Time");
		meta.add(reqField);
		filterTables.add(new CompareFilterPanel<String, TimeField>(reqField));
		
		startField = new TimeField("Delivery Start Time");
		meta.add(startField);
		filterTables.add(new CompareFilterPanel<String, TimeField>(startField));
		
		finField = new TimeField("Delivery Finish Time");
		meta.add(finField);
		filterTables.add(new CompareFilterPanel<String, TimeField>(finField));
		
		String[] statusName = {
			OrderStatus.REQUESTED.getTypeName(),
			OrderStatus.PAYED.getTypeName(),
			OrderStatus.DELIVERING.getTypeName(),
			OrderStatus.DELIVERED.getTypeName(),
			OrderStatus.COMPLETED.getTypeName()
		};
		statusField = new DropdownField("Delivery Status", statusName);
		meta.add(statusField);
		filterTables.add(new SelectionFilterPanel<DropdownField>(statusField));
		
		table = new DataTable(meta);
		
		if(managerAccount.getAccountType() == AccountType.SP) {
			proviField.addRangeFilter(managerAccount.getID(), managerAccount.getID(), true);
			proviField.setVisible(false);
		} else if(managerAccount.getAccountType() == AccountType.SC) {
			custField.addRangeFilter(managerAccount.getID(), managerAccount.getID(), true);
			custField.setVisible(false);
		}
		
	}
	
	public DataRecord getRecord(Order or) throws InvalidDataException {
		DataRecord dr = new DataRecord(or.getID());
		dr.setField(proviField, or.getProviderID());
		dr.setField(custField, or.getCustomerID());
		dr.setField(foodField, or.getFoodID());
		dr.setField(feeField, or.getFee());
		dr.setField(reqField, or.getRequestTime());
		dr.setField(startField, or.getStartTime());
		dr.setField(finField, or.getFinishTime());
		dr.setField(statusField, or.getOrderStatus().getTypeName());
		return dr;
	}

	@Override
	public Order getInstance(String[] str) throws InvalidDataException {
		return new Order(str);
	}
	
}
