package hk.hku.learner.oop.food;

public enum OrderStatus {
	REQUESTED("Requested"),
	PAYED("Payment Accepted"),
	DELIVERING("Delivering"),
	DELIVERED("Delivered"),
	COMPLETED("Completed");
	
	private final String typeName;
	
	private OrderStatus(String name) {
		typeName = name;
	}
	
	public String getTypeName() {
		return typeName;
	}
	
	public static OrderStatus getStatus(String status) {
		if(status.equals(REQUESTED.getTypeName()))
			return REQUESTED;
		else if(status.equals(PAYED.getTypeName()))
			return PAYED;
		else if(status.equals(DELIVERING.getTypeName()))
			return DELIVERING;
		else if(status.equals(DELIVERED.getTypeName()))
			return DELIVERED;
		else
			return COMPLETED;
	}

}
