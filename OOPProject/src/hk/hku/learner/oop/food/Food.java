package hk.hku.learner.oop.food;

import hk.hku.learner.oop.database.Identifiable;
import hk.hku.learner.oop.database.field.InvalidDataException;

public class Food implements Identifiable {

	private int id, providerID, delivTime;
	private String name, decrip, fee;

	public Food(String[] str) throws InvalidDataException {
		if (str.length != 6)
			throw new InvalidDataException("String[6]", "String[" + str.length + "]");
		try {
			id = Integer.parseInt(str[0]);
		} catch (NumberFormatException e) {
			throw new InvalidDataException("Integer ID", str[0].equals("") ? "Empty String" : "String " + str[0]);
		}
		try {
			providerID = Integer.parseInt(str[1]);
		} catch (NumberFormatException e) {
			throw new InvalidDataException("Integer ID", str[1].equals("") ? "Empty String" : "String " + str[1]);
		}
		name = str[2];
		decrip = str[3];
		try {
			delivTime = Integer.parseInt(str[4]);
		} catch (NumberFormatException e) {
			throw new InvalidDataException("Integer Time", str[4].equals("") ? "Empty String" : "String " + str[4]);
		}
		fee = str[5];
	}

	@Override
	public int getID() {
		return id;
	}

	public Integer getProvider() {
		return providerID;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return decrip;
	}

	public Integer getDeliverTime() {
		return delivTime;
	}

	public String getFee() {
		return fee;
	}

}
