package hk.hku.learner.oop.food;

import java.util.ArrayList;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.account.AccountType;
import hk.hku.learner.oop.database.DataManager;
import hk.hku.learner.oop.database.DataRecord;
import hk.hku.learner.oop.database.DataTable;
import hk.hku.learner.oop.database.field.Field;
import hk.hku.learner.oop.database.field.IntegerField;
import hk.hku.learner.oop.database.field.InvalidDataException;
import hk.hku.learner.oop.database.field.PatternField;
import hk.hku.learner.oop.database.field.StringField;
import hk.hku.learner.oop.gui.filter.CompareFilterPanel;
import hk.hku.learner.oop.gui.filter.PatternFilterPanel;

public class FoodManager extends DataManager<Food> {
	
	private IntegerField proviField, delivTimeField;
	private StringField nameField, descField;
	private PatternField feeField;
	
	public FoodManager(Account managerAccount) {
		ArrayList<Field<?>> meta = new ArrayList<Field<?>>();
		
		proviField = new IntegerField("Provider ID", 100000, 199999);
		meta.add(proviField);
		filterTables.add(new CompareFilterPanel<Integer, IntegerField>(proviField));
		
		nameField = new StringField("Name");
		meta.add(nameField);
		filterTables.add(new PatternFilterPanel<StringField>(nameField));
		
		descField = new StringField("Description");
		meta.add(descField);
		filterTables.add(new PatternFilterPanel<StringField>(descField));
		
		delivTimeField = new IntegerField("Deliver Time (mins)", 1, 360);
		meta.add(delivTimeField);
		filterTables.add(new CompareFilterPanel<Integer, IntegerField>(delivTimeField));
		
		feeField = new PatternField("Fee (HKD)", "[0-9]*.[0-9]{2}");
		meta.add(feeField);
		filterTables.add(new CompareFilterPanel<String, PatternField>(feeField));
		
		if(managerAccount.getAccountType() == AccountType.SP) {
			proviField.addEqualsFilter(managerAccount.getID(), true);
			proviField.setEditable(false);
		} else if(managerAccount.getAccountType() == AccountType.SC) {
			proviField.setEditable(false);
			nameField.setEditable(false);
			descField.setEditable(false);
			delivTimeField.setEditable(false);
			feeField.setEditable(false);
		}
		
		table = new DataTable(meta);
		
	}

	@Override
	public DataRecord getRecord(Food fd) throws InvalidDataException {
		DataRecord dr = new DataRecord(fd.getID());
		dr.setField(proviField, fd.getProvider());
		dr.setField(nameField, fd.getName());
		dr.setField(descField, fd.getDescription());
		dr.setField(delivTimeField, fd.getDeliverTime());
		dr.setField(feeField, fd.getFee());
		return dr;
	}

	@Override
	public Food getInstance(String[] str) throws InvalidDataException {
		return new Food(str);
	}

}
