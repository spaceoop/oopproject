package hk.hku.learner.oop.database;

public interface Identifiable {
	public int getID();
}
