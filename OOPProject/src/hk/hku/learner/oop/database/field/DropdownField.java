package hk.hku.learner.oop.database.field;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.table.TableColumn;

public class DropdownField extends StringField {
	
	private String[] dropOption;

	public DropdownField(String name, String[] options) {
		super(name);
		dropOption = options;
	}
	
	public String[] getDropOption() {
		return dropOption;
	}
	
	@Override
	public void setEditor(TableColumn tc) {
		tc.setCellEditor(new DefaultCellEditor(new JComboBox<String>(dropOption)));
	}

}
