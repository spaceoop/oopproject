package hk.hku.learner.oop.database.field;

public class DateField extends PatternField {

	public DateField(String name) {
		super(name, "[0-9]{2}-[0-9]{2}-[0-9]{4}");
	}

}
