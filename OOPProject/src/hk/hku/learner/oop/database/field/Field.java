package hk.hku.learner.oop.database.field;

import javax.swing.table.TableColumn;

import hk.hku.learner.oop.database.filter.EqualsMatcher;
import hk.hku.learner.oop.database.filter.Filter;

public abstract class Field<T> {
	private final String fieldName;
	private boolean editable;
	private boolean visible;
	
	protected Filter<T> dispFilter;
	
	public Field(String name, boolean canEdit, boolean isVisible) {
		fieldName = name;
		dispFilter = new Filter<T>();
		setEditable(canEdit);
		setVisible(isVisible);
	}

	public Field(String name) {
		this(name, true, true);
	}
	
	public String getName() {
		return fieldName;
	}
	
	public abstract Class<?> getFieldClass();
	
	/**
	 * Parse the object to the format specified in the field.
	 * @param o the input object.
	 * @return the parsed object.
	 * @throws InvalidDataException
	 */
	public abstract T parseValue(Object o) throws InvalidDataException;
	
	public boolean objectFilter(Object o) throws InvalidDataException {
		return matchesFilter(parseValue(o));
	}
	
	/**
	 * Check whether the object matches the filter
	 * @param o the input object.
	 * @return true if the object matches the filter
	 */
	public boolean matchesFilter(T o) {
		return dispFilter.matches(o);
	}
	
	public void resetFilter() {
		dispFilter.reset();
	}
	
	public void setEditor(TableColumn tc) {
		// Use default editor
	}
	
	public boolean isEditable() {
		return editable;
	}
	
	public boolean isVisible() {
		return visible;
	}
	
	public void setEditable(boolean canEdit) {
		editable = canEdit;
	}

	public void setVisible(boolean isVisible) {
		visible = isVisible;
	}
	
	public void addEqualsFilter(T val, boolean sticky) {
		dispFilter.addMatcher(new EqualsMatcher<T>(val), sticky);
	}

}
