package hk.hku.learner.oop.database.field;

public class TimeField extends PatternField {

	public TimeField(String name) {
		super(name, "[0-9]{2}-[0-9]{2}-[0-9]{4} [0-9]{2}:[0-9]{2}");
	}

}
