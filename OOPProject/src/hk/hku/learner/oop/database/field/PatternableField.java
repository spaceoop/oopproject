package hk.hku.learner.oop.database.field;

import java.util.regex.Pattern;

public interface PatternableField {
	void addPatternFilter(Pattern pattern, boolean sticky);
	void addPatternFilter(String pattern, boolean sticky);
}
