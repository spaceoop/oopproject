package hk.hku.learner.oop.database.field;

import java.util.regex.Pattern;

public class PatternField extends StringField {

	Pattern pat;
	
	public PatternField(String name, Pattern pattern) {
		super(name);
		pat = pattern;
	}
	
	public PatternField(String name, String pattern) {
		this(name, Pattern.compile(pattern));
	}

	@Override
	public String parseValue(Object o) throws InvalidDataException {
		if(o instanceof String){
			String str = (String) o;
			if(!pat.matcher(str).matches()) 
				throw new InvalidDataException("String with pattern " + pat.pattern(), str);
			return str;
		} else {
			throw new InvalidDataException("String", o.getClass().getName());
		}
	}
	
}
