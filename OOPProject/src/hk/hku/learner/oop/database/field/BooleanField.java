package hk.hku.learner.oop.database.field;

public class BooleanField extends Field<Boolean> {

	public BooleanField(String name) {
		super(name);
	}

	@Override
	public Boolean parseValue(Object o) throws InvalidDataException {
		if (o instanceof Boolean) {
			Boolean val = (Boolean) o;
			return val;
		} else if(o instanceof String){
			try {
				Boolean val = new Boolean((String) o);
				return val;
			} catch (NumberFormatException e) {
				throw new InvalidDataException("Boolean", "String");
			}
		} else {
			throw new InvalidDataException("Boolean", o.getClass().getName());
		}
	}
	
	@Override
	public Class<?> getFieldClass() {
		return Boolean.class;
	}

}
