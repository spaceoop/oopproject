package hk.hku.learner.oop.database.field;

public interface ComparableField<T extends Comparable<? super T>> {
	void addLessThanFilter(T x, boolean sticky);
	void addGreaterThanFilter(T x, boolean sticky);
	void addLessThanEqualsFilter(T x, boolean sticky);
	void addGreaterThanEqualsFilter(T x, boolean sticky);
	default void addRangeFilter(T from, T to, boolean sticky) {
		addGreaterThanEqualsFilter(from, sticky);
		addLessThanEqualsFilter(to, sticky);
	}
}
