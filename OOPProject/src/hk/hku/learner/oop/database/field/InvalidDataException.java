package hk.hku.learner.oop.database.field;

public class InvalidDataException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3723851239812082565L;
	
	public InvalidDataException(String expected, String found){
		super(expected + " expected, but " + found + " found.");
	}

}
