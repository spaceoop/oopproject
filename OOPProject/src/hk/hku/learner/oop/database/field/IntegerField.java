package hk.hku.learner.oop.database.field;

import hk.hku.learner.oop.database.filter.*;

public class IntegerField extends Field<Integer> implements ComparableField<Integer>{
	
	private int minValue, maxValue;
	
	public IntegerField(String name, int min, int max) {
		super(name);
		minValue = min;
		maxValue = max;
	}

	@Override
	public Integer parseValue(Object o) throws InvalidDataException {
		if (o instanceof Integer) {
			Integer val = (Integer) o;
			if(val < minValue || val > maxValue) {
				throw new InvalidDataException("Integer in range [" + minValue + ".." + maxValue + "]", val.toString());
			}
			return val;
		} else if(o instanceof String){
			try {
				Integer val = new Integer((String) o);
				if(val < minValue || val > maxValue) {
					throw new InvalidDataException("Integer in range [" + minValue + ".." + maxValue + "]", val.toString());
				}
				return val;
			} catch (NumberFormatException e) {
				throw new InvalidDataException("Integer", "String");
			}
		} else {
			throw new InvalidDataException("Integer", o.getClass().getName());
		}
		
	}

	@Override
	public Class<?> getFieldClass() {
		return Integer.class;
	}

	@Override
	public void addLessThanFilter(Integer x, boolean sticky) {
		dispFilter.addMatcher(new SmallerMatcher<Integer>(x), sticky);
	}

	@Override
	public void addGreaterThanFilter(Integer x, boolean sticky) {
		dispFilter.addMatcher(new GreaterMatcher<Integer>(x), sticky);
	}

	@Override
	public void addLessThanEqualsFilter(Integer x, boolean sticky) {
		dispFilter.addMatcher(new SmallerEqualsMatcher<Integer>(x), sticky);
	}

	@Override
	public void addGreaterThanEqualsFilter(Integer x, boolean sticky) {
		dispFilter.addMatcher(new GreaterEqualsMatcher<Integer>(x), sticky);
	}

}
