package hk.hku.learner.oop.database.field;

import java.util.regex.Pattern;

import hk.hku.learner.oop.database.filter.*;

public class StringField extends Field<String> implements ComparableField<String>, PatternableField {

	public StringField(String name) {
		super(name);
	}

	@Override
	public Class<?> getFieldClass() {
		return String.class;
	}

	@Override
	public String parseValue(Object o) throws InvalidDataException {
		if(o instanceof String){
			return (String) o;
		} else {
			throw new InvalidDataException("String", o.getClass().getName());
		}
	}

	@Override
	public void addLessThanFilter(String x, boolean sticky) {
		dispFilter.addMatcher(new SmallerMatcher<String>(x), sticky);
	}

	@Override
	public void addGreaterThanFilter(String x, boolean sticky) {
		dispFilter.addMatcher(new GreaterMatcher<String>(x), sticky);
	}

	@Override
	public void addLessThanEqualsFilter(String x, boolean sticky) {
		dispFilter.addMatcher(new SmallerEqualsMatcher<String>(x), sticky);
	}

	@Override
	public void addGreaterThanEqualsFilter(String x, boolean sticky) {
		dispFilter.addMatcher(new GreaterEqualsMatcher<String>(x), sticky);
	}

	@Override
	public void addPatternFilter(Pattern pattern, boolean sticky) {
		dispFilter.addMatcher(new PatternMatcher(pattern), sticky);
	}

	@Override
	public void addPatternFilter(String pattern, boolean sticky) {
		dispFilter.addMatcher(new PatternMatcher(pattern), sticky);
	}
	

}
