package hk.hku.learner.oop.database;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import hk.hku.learner.oop.database.field.InvalidDataException;
import hk.hku.learner.oop.gui.filter.FilterPanel;
import hk.hku.learner.oop.utils.FileIO;

public abstract class DataManager<T extends Identifiable> {

	protected DataTable table;
	protected ArrayList<FilterPanel<?, ?>> filterTables;

	protected DataManager() {
		filterTables = new ArrayList<FilterPanel<?, ?>>();
	}

	public abstract DataRecord getRecord(T rec) throws InvalidDataException;

	public abstract T getInstance(String[] str) throws InvalidDataException;

	public void addRecord(T rec) throws InvalidDataException {
		table.addRecord(getRecord(rec));
	}

	public void removeRecord(int id) throws RecordNotFoundException {
		table.removeRecord(id);
	}

	public void removeRecord(T rec) throws RecordNotFoundException {
		removeRecord(rec.getID());
	}

	public void updateRecord(T rec) throws InvalidDataException, RecordNotFoundException {
		removeRecord(rec);
		addRecord(rec);
	}

	public int getNextAviliableID() {
		return table.getNextAviliableID();
	}

	public void loadRecord(String filename) {
		System.out.println("Reading record from file: " + filename);
		ArrayList<String[]> impRec = FileIO.readTxtfile(filename);
		for (int i = 1; i < impRec.size(); i++) {
			try {
				addRecord(getInstance(impRec.get(i)));
			} catch (InvalidDataException e) {
				String errMsg = "ERROR: Invalid record from file \"" + filename + "\" at line " + i
						+ ". Record skipped.\n" + e.getMessage();
				JOptionPane.showMessageDialog(null, errMsg, "Invalid File Data!", JOptionPane.WARNING_MESSAGE);
				System.err.println(errMsg);
			}
		}
	}

	public void storeRecord(String filename) {
		System.out.println("Writing record to file: " + filename);
		FileIO.writeTxtfile(filename, table.getRecordListString());
	}

	public DataTable getTable() {
		return table;
	}

	public ArrayList<FilterPanel<?, ?>> getFilterPanels() {
		return filterTables;
	}

	public T getRecordInstance(int id) throws RecordNotFoundException {
		String[] hack = table.getRecordString(id);
		try {
			return getInstance(hack);
		} catch (InvalidDataException e) {
			throw new RecordNotFoundException();
		}
	}

}
