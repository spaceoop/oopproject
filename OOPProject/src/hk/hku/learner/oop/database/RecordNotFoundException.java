package hk.hku.learner.oop.database;

public class RecordNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7190945649662755199L;
	
	public RecordNotFoundException() {
		super("record not found in table");
	}

}
