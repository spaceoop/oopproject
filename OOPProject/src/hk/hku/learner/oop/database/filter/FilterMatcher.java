package hk.hku.learner.oop.database.filter;

public interface FilterMatcher<T> {
	boolean matches(T o);
}
