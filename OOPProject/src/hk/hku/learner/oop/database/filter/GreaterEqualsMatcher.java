package hk.hku.learner.oop.database.filter;

public class GreaterEqualsMatcher<T extends Comparable<? super T>> extends ComparisonMatcher<T> {

	public GreaterEqualsMatcher(T criteria) {
		super(criteria);
	}

	@Override
	public boolean compare(T arg1, T arg2) {
		return arg1.compareTo(arg2) >= 0;
	}

}
