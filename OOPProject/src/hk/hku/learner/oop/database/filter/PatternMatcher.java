package hk.hku.learner.oop.database.filter;

import java.util.regex.Pattern;

public class PatternMatcher implements FilterMatcher<String> {
	
	private Pattern pat;
	
	public PatternMatcher(Pattern pattern) {
		pat = pattern;
	}
	
	public PatternMatcher(String pattern) {
		this(Pattern.compile(pattern));
	}

	@Override
	public boolean matches(String o) {
		return pat.matcher(o).matches();
	}

}
