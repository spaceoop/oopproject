package hk.hku.learner.oop.database.filter;

import java.util.ArrayList;

public class Filter<T> {
	
	private ArrayList<FilterMatcher<T>> filts, stickyFilts;
	
	public Filter() {
		stickyFilts = new ArrayList<FilterMatcher<T>>();
		reset();
	}
	
	public void addMatcher(FilterMatcher<T> matcher, boolean sticky) {
		if(sticky)
			stickyFilts.add(matcher);
		else
			filts.add(matcher);
	}
	
	public void reset() {
		filts = new ArrayList<FilterMatcher<T>>();
	}
	
	public boolean matches(T obj) {
		for(FilterMatcher<T> fm: filts) {
			if(!fm.matches(obj))
				return false;
		}
		for(FilterMatcher<T> fm: stickyFilts) {
			if(!fm.matches(obj))
				return false;
		}
		return true;
	}
}
