package hk.hku.learner.oop.database.filter;

public class EqualsMatcher<T> implements FilterMatcher<T> {
	
	private T val;
	
	public EqualsMatcher(T criteria) {
		val = criteria;
	}

	@Override
	public boolean matches(T o) {
		return o.equals(val);
	}

}
