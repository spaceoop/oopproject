package hk.hku.learner.oop.database.filter;

public abstract class ComparisonMatcher<T extends Comparable<? super T>> implements FilterMatcher<T> {
	
	private T val;
	
	public ComparisonMatcher(T criteria) {
		val = criteria;
	}
	
	public abstract boolean compare(T arg1, T arg2);

	@Override
	public boolean matches(T o) {
		return compare(o, val);			
	}

}
