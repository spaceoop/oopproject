package hk.hku.learner.oop.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import hk.hku.learner.oop.database.field.Field;
import hk.hku.learner.oop.database.field.InvalidDataException;

public class DataTableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 710865924771787493L;

	private ArrayList<Field<?>> meta;
	private ArrayList<DataRecord> rec, displayRec;
	private Map<Integer, DataRecord> recID;
	private int nextAviliableID;

	public DataTableModel(ArrayList<Field<?>> metadata) {
		meta = metadata;
		recID = new HashMap<Integer, DataRecord>();
		rec = new ArrayList<DataRecord>();
		nextAviliableID = 0;
		resetDisplay();
	}

	@Override
	public String getColumnName(int columnIndex) {
		if (columnIndex > 0)
			return getField(columnIndex - 1).getName();
		else
			return "ID";
	}

	@Override
	public int getRowCount() {
		return displayRec.size();
	}

	@Override
	public int getColumnCount() {
		return meta.size() + 1;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		DataRecord recRow = displayRec.get(rowIndex);
		if (columnIndex > 0) {
			Field<?> recCol = getField(columnIndex - 1);
			return recRow.getField(recCol);
		} else {
			return Integer.valueOf(recRow.getID());
		}
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		DataRecord recRow = displayRec.get(rowIndex);
		Field<?> recCol = getField(columnIndex - 1);
		try {
			recRow.setField(recCol, aValue);
			fireTableCellUpdated(rowIndex, columnIndex);
		} catch (InvalidDataException e) {
			JOptionPane.showMessageDialog(null,
					"Please enter the data according to the format.\nError: " + e.getMessage(), "Invalid Data!",
					JOptionPane.WARNING_MESSAGE);
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (columnIndex > 0) {
			return getField(columnIndex - 1).isEditable();
		} else {
			return false;
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		if (columnIndex > 0)
			return getField(columnIndex - 1).getFieldClass();
		else
			return Integer.class;
	}

	@Override
	public String toString() {
		return rec.toString();
	}

	public void addRecord(DataRecord record) {
		rec.add(record);
		recID.put(record.getID(), record);
		updateAvliableID(record.getID());
		resetDisplay();
	}

	public void removeRecord(int id) throws RecordNotFoundException {
		removeRecord(getRecord(id));
	}

	public void removeRecord(DataRecord record) {
		recID.remove(record.getID());
		rec.remove(record);
		resetDisplay();
	}

	public DataRecord getRecord(int id) throws RecordNotFoundException {
		DataRecord rt = recID.get(id);
		if (rt == null)
			throw new RecordNotFoundException();
		return rt;
	}

	public Field<?> getField(int id) {
		return meta.get(id);
	}

	public int searchRecord(Field<?> fe, Object o) throws RecordNotFoundException {
		for (int i = 0; i < rec.size(); i++) {
			DataRecord dr = rec.get(i);
			if (dr.getField(fe).equals(o))
				return dr.getID();
		}
		throw new RecordNotFoundException();
	}

	public String[] getRecordString(int id) throws RecordNotFoundException {
		return getRecordString(getRecord(id));
	}

	public String[] getRecordString(DataRecord dr) {
		String[] rt = new String[meta.size() + 1];
		rt[0] = Integer.toString(dr.getID());
		for (int i = 0; i < meta.size(); i++)
			rt[i + 1] = dr.getField(meta.get(i)).toString();
		return rt;
	}

	public ArrayList<String[]> getRecordListString() {
		ArrayList<String[]> outRec = new ArrayList<String[]>();
		String[] header = new String[meta.size() + 1];
		header[0] = "ID";
		for (int i = 0; i < meta.size(); i++)
			header[i + 1] = meta.get(i).getName();
		outRec.add(header);
		for (DataRecord dr : rec)
			outRec.add(getRecordString(dr));
		return outRec;
	}

	public void resetDisplay() {
		displayRec = new ArrayList<DataRecord>();
		for (DataRecord dr : rec)
			if (dr.matchesFilter())
				displayRec.add(dr);
		fireTableDataChanged();
	}

	public void updateAvliableID(int id) {
		nextAviliableID = Math.max(nextAviliableID, id) + 1;
	}

	public int getNextAviliableID() {
		int rt = nextAviliableID;
		updateAvliableID(nextAviliableID);
		return rt;
	}

}
