package hk.hku.learner.oop.database;

import java.util.HashMap;
import java.util.Map;
import hk.hku.learner.oop.database.field.Field;
import hk.hku.learner.oop.database.field.InvalidDataException;

public class DataRecord {
	private final int id;
	private Map<Field<?>, Object> data;

	public DataRecord(int uid) {
		id = uid;
		data = new HashMap<Field<?>, Object>();
	}

	public void setField(Field<?> f, Object o) throws InvalidDataException {
		data.put(f, f.parseValue(o));
	}

	public Object getField(Field<?> f) {
		return data.get(f);
	}

	public int getID() {
		return id;
	}

	public boolean matchesFilter() {
		for (Map.Entry<Field<?>, Object> pr : data.entrySet()) {
			Field<?> fe = pr.getKey();
			try {
				if (!fe.objectFilter(pr.getValue()))
					return false;
			} catch (InvalidDataException e) {
				return false;
			}
		}
		return true;
	}

}
