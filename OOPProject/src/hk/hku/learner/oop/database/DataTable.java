package hk.hku.learner.oop.database;

import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.TableColumn;

import hk.hku.learner.oop.database.field.Field;

public class DataTable extends JTable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6137007267562837572L;

	private DataTableModel dtm;

	private DataTable(DataTableModel tableModel) {
		super(tableModel);
		dtm = tableModel;

		TableColumn[] dc = new TableColumn[dtm.getColumnCount() - 1];

		for (int i = 0; i < dtm.getColumnCount() - 1; i++)
			dc[i] = getColumnModel().getColumn(i + 1);

		for (int i = 0; i < dtm.getColumnCount() - 1; i++) {
			Field<?> fie = dtm.getField(i);
			fie.setEditor(dc[i]);
			if (!fie.isVisible())
				removeColumn(dc[i]);
		}

	}

	public DataTable(ArrayList<Field<?>> metadata) {
		this(new DataTableModel(metadata));
	}

	public void addRecord(DataRecord recordEntry) {
		dtm.addRecord(recordEntry);
	}

	public void removeRecord(int id) throws RecordNotFoundException {
		dtm.removeRecord(id);
	}

	public DataRecord getRecord(int id) throws RecordNotFoundException {
		return dtm.getRecord(id);
	}

	public Field<?> getField(int id) {
		return dtm.getField(id);
	}

	public int searchRecord(Field<?> fe, Object o) throws RecordNotFoundException {
		return dtm.searchRecord(fe, o);
	}

	public String[] getRecordString(DataRecord dr) {
		return dtm.getRecordString(dr);
	}

	public String[] getRecordString(int id) throws RecordNotFoundException {
		return dtm.getRecordString(id);
	}

	public ArrayList<String[]> getRecordListString() {
		return dtm.getRecordListString();
	}

	public void resetDisplay() {
		dtm.resetDisplay();
	}

	public int getNextAviliableID() {
		return dtm.getNextAviliableID();
	}

}
