package hk.hku.learner.oop.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public final class FileIO {

	private FileIO() {
	}

	public static ArrayList<String[]> readTxtfile(String dataFile) {
		ArrayList<String[]> dataList = new ArrayList<String[]>();
		File inFile = new File(dataFile);
		try {
			inFile.createNewFile();
		} catch (IOException e1) {
			System.err.println("ERROR: Fail to create file");
		}
		try {
			Scanner sc = new Scanner(inFile);
			while (sc.hasNextLine()) {
					dataList.add(StringUtils.mySplit(sc.nextLine()));
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.err.println("ERROR: File not found.");
		}
		return dataList;
	}

	public static void writeTxtfile(String outFile, ArrayList<String[]> outDataList) {
		File outputFile = new File(outFile);
		try {
			outputFile.createNewFile();
		} catch (IOException e1) {
			System.err.println("ERROR: Fail to create file");
		}
		try {
			PrintWriter writer = new PrintWriter(outputFile);
			for (String[] i : outDataList) {
				for (int j = 0; j < i.length; j++)
					if (i[j].contains(","))
						i[j] = '"' + i[j] + '"';
				writer.println(String.join(",", i));
			}
			writer.close();
		} catch (FileNotFoundException e) {
			System.err.println("ERROR: File not found.");
		}
	}

	public static void main(String[] args) {
		ArrayList<String[]> dam = readTxtfile("testData");
		ArrayList<String[]> outLine = new ArrayList<String[]>();
		for (int i = 0; i < dam.size(); i++) {
			String[] yes = dam.get(i);
			outLine.add(yes);
			for (int j = 0; j < yes.length; j++) {
				System.out.println("position[" + i + "][" + j + "] is " + yes[j]);
			}
		}
		FileIO.writeTxtfile("testData2", outLine);
	}
}
