package hk.hku.learner.oop.utils;

public final class StringUtils {

	private StringUtils() {
	}

	public static String[] mySplit(String input) {
		int poS = 0;
		String[] outputLine = new String[10];
		for (int i = 0; i < 10; i++)
			outputLine[i] = "";
		char[] readLine1 = input.toCharArray();
		int length = readLine1.length;
		for (int poC = 0; poC < length; poC++) {
			if (readLine1[poC] == '"') {
				poC++;
				while (readLine1[poC] != '"') {
					outputLine[poS] = outputLine[poS] + readLine1[poC];
					poC++;
				}
			} else if (readLine1[poC] == ',') {
				poS++;
			} else {
				outputLine[poS] = outputLine[poS] + readLine1[poC];
			}
		}
		String[] realopLine = new String[poS + 1];
		for (int i = 0; i <= poS; i++) {
			realopLine[i] = outputLine[i];
		}
		return realopLine;
	}

}
