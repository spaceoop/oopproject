package hk.hku.learner.oop.gui.filter;

import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.regex.PatternSyntaxException;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import hk.hku.learner.oop.database.DataManager;
import hk.hku.learner.oop.database.field.InvalidDataException;
import hk.hku.learner.oop.gui.utils.TextPanel;

public class FilterView extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9142621311046563727L;

	private DataManager<?> am;
	private DefaultListModel<String> filterList;
	private JComboBox<String> jcb;

	public FilterView(DataManager<?> manager) {

		am = manager;

		setLayout(new GridBagLayout());
		setBorder(new EmptyBorder(10, 10, 10, 10));

		GridBagConstraints gbc = new GridBagConstraints();

		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;

		TextPanel newPanel = new AddPanel();
		add(newPanel, gbc);

		gbc.fill = GridBagConstraints.NONE;
		gbc.gridx = 1;
		gbc.weightx = 0.0;

		JButton addButton = new JButton(">>");
		addButton.addActionListener((event) -> addFilter());
		add(addButton, gbc);

		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = 2;
		gbc.weightx = 1.0;

		filterList = new DefaultListModel<String>();
		JList<String> filterListComp = new JList<String>(filterList);
		filterList.addElement("[No Filter]");
		filterListComp.setFixedCellWidth(200);

		JButton clearButton = new JButton("Clear");
		clearButton.addActionListener((event) -> clearFilter());

		TextPanel listPanel = new ListPanel(new JScrollPane(filterListComp), clearButton);
		add(listPanel, gbc);

	}

	private void addFilter() {
		if (filterList.getElementAt(0).equals("[No Filter]"))
			filterList.clear();
		for (FilterPanel<?, ?> fp : am.getFilterPanels()) {
			if (fp.getFieldName().equals((String) jcb.getSelectedItem())) {
				try {
					filterList.addElement(fp.applyFilter());
				} catch (InvalidDataException e) {
					JOptionPane.showMessageDialog(null, "Filter value does not match the format:\n" + e.getMessage(),
							"Invalid Filter", JOptionPane.ERROR_MESSAGE);
				} catch (PatternSyntaxException e) {
					JOptionPane.showMessageDialog(null, "Filter regex pattern invalid:\n" + e.getMessage(),
							"Invalid Filter", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		am.getTable().resetDisplay();
		if (filterList.isEmpty())
			filterList.addElement("[No Filter]");
	}

	private void clearFilter() {
		filterList.clear();
		for (FilterPanel<?, ?> fp : am.getFilterPanels()) {
			fp.resetFilter();
		}
		am.getTable().resetDisplay();
		filterList.addElement("[No Filter]");
	}

	private class ListPanel extends TextPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5571151155988308478L;

		public ListPanel(JComponent list, JButton button) {
			super("Filter List");
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(5, 5, 5, 5);

			gbc.fill = GridBagConstraints.BOTH;
			gbc.gridx = 0;
			gbc.gridy = 0;
			gbc.weightx = 1.0;
			gbc.weighty = 1.0;
			add(list, gbc);

			gbc.fill = GridBagConstraints.NONE;
			gbc.gridx = 0;
			gbc.gridy = 1;
			gbc.weighty = 0.0;
			add(button, gbc);
		}

	}

	private class AddPanel extends TextPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5177774118191636011L;

		private JPanel jp;

		public AddPanel() {
			super("New Filter");

			setLayout(new GridBagLayout());

			GridBagConstraints gbc = new GridBagConstraints();

			jcb = new JComboBox<String>();
			jcb.addActionListener((event) -> updateCard());

			jp = new JPanel(new CardLayout());
			for (FilterPanel<?, ?> fp : am.getFilterPanels()) {
				jp.add(fp, fp.getFieldName());
				jcb.addItem(fp.getFieldName());
			}

			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.gridx = 0;
			gbc.gridy = 0;
			gbc.weightx = 1.0;
			add(jcb, gbc);

			gbc.fill = GridBagConstraints.BOTH;
			gbc.gridy = 1;
			gbc.weighty = 1.0;
			add(jp, gbc);

		}

		private void updateCard() {
			CardLayout cl = (CardLayout) jp.getLayout();
			cl.show(jp, (String) jcb.getSelectedItem());
		}

	}

}
