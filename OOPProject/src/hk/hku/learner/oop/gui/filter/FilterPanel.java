package hk.hku.learner.oop.gui.filter;

import javax.swing.JPanel;

import hk.hku.learner.oop.database.field.Field;
import hk.hku.learner.oop.database.field.InvalidDataException;

public abstract class FilterPanel <T, U extends Field<T>> extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5992123357492623833L;
	
	protected U field;
	
	public FilterPanel(U filterField) {
		field = filterField;
	}
	
	public abstract String applyFilter() throws InvalidDataException;
	
	public void resetFilter() {
		field.resetFilter();
	}
	
	public String getFieldName() {
		return field.getName();
	}

}
