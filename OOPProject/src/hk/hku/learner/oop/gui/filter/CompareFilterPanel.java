package hk.hku.learner.oop.gui.filter;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import hk.hku.learner.oop.database.field.ComparableField;
import hk.hku.learner.oop.database.field.Field;
import hk.hku.learner.oop.database.field.InvalidDataException;

public class CompareFilterPanel<T extends Comparable<? super T>, U extends Field<T> & ComparableField<T>>
		extends FilterPanel<T, U> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1110817825672503850L;

	private JTextField ltField, gtField, leqField, geqField, fromField, toField, eqField;
	private JRadioButton ltButton, gtButton, leqButton, geqButton, rangeButton, eqButton;

	public CompareFilterPanel(U compareField) {
		
		super(compareField);

		setLayout(new GridBagLayout());

		ltButton = new JRadioButton("<");
		gtButton = new JRadioButton(">");
		leqButton = new JRadioButton("<=", true);
		geqButton = new JRadioButton(">=");
		eqButton = new JRadioButton("==");
		rangeButton = new JRadioButton("range");

		ButtonGroup bg = new ButtonGroup();
		bg.add(ltButton);
		bg.add(leqButton);
		bg.add(geqButton);
		bg.add(gtButton);
		bg.add(eqButton);
		bg.add(rangeButton);

		ltField = new JTextField();
		gtField = new JTextField();
		leqField = new JTextField();
		geqField = new JTextField();
		fromField = new JTextField();
		eqField = new JTextField();
		toField = new JTextField();

		GridBagConstraints gbc = new GridBagConstraints();

		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(5, 5, 5, 3);

		gbc.gridx = 0;
		gbc.gridy = 0;
		add(leqButton, gbc);
		gbc.gridy = 1;
		add(geqButton, gbc);
		gbc.gridy = 2;
		add(eqButton, gbc);

		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 0.5;
		gbc.insets = new Insets(5, 0, 5, 5);

		gbc.gridx = 1;
		gbc.gridy = 0;
		add(leqField, gbc);
		gbc.gridy = 1;
		add(geqField, gbc);
		gbc.gridy = 2; gbc.gridwidth = 3;
		add(eqField, gbc);

		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0.0; gbc.gridwidth = 1;
		gbc.insets = new Insets(5, 5, 5, 3);

		gbc.gridx = 2;
		gbc.gridy = 0;
		add(ltButton, gbc);
		gbc.gridy = 1;
		add(gtButton, gbc);

		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 0.5;
		gbc.insets = new Insets(5, 0, 5, 5);

		gbc.gridx = 3;
		gbc.gridy = 0;
		add(ltField, gbc);
		gbc.gridy = 1;
		add(gtField, gbc);
		
		JPanel rangeP = new JPanel(new GridBagLayout());
		GridBagConstraints gbd = new GridBagConstraints();	
		gbd.gridx = gbd.gridy = 0;
		gbd.insets = new Insets(0, 0, 0, 3);
		rangeP.add(rangeButton, gbd);
		gbd.gridx = 2;
		gbd.insets = new Insets(0, 3, 0, 3);
		rangeP.add(new JLabel("-"), gbd);
		gbd.insets = new Insets(0, 0, 0, 0);
		gbd.fill = GridBagConstraints.HORIZONTAL;
		gbd.weightx = 1.0;
		gbd.gridx = 1;
		rangeP.add(fromField, gbd);
		gbd.gridx = 3;
		rangeP.add(toField, gbd);
		
		gbc.gridx = 0; gbc.gridy = 3; gbc.gridwidth = 4;
		gbc.insets = new Insets(5, 5, 5, 5);
		add(rangeP, gbc);

		gbc.gridy = 4;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weighty = 1.0;
		add(new JLabel(), gbc);

	}
	
	@Override
	public String applyFilter() throws InvalidDataException {
		if (ltButton.isSelected()) {
			T val = field.parseValue(ltField.getText());
			field.addLessThanFilter(val, false);
			return field.getName() + ": < " + val.toString();
		} else if (gtButton.isSelected()) {
			T val = field.parseValue(gtField.getText());
			field.addGreaterThanFilter(val, false);
			return field.getName() + ": > " + val.toString();
		} else if (leqButton.isSelected()) {
			T val = field.parseValue(leqField.getText());
			field.addLessThanEqualsFilter(val, false);
			return field.getName() + ": <= " + val.toString();
		} else if (geqButton.isSelected()) {
			T val = field.parseValue(geqField.getText());
			field.addGreaterThanEqualsFilter(val, false);
			return field.getName() + ": >= " + val.toString();
		} else if (rangeButton.isSelected()) {
			T valF = field.parseValue(fromField.getText());
			T valT = field.parseValue(toField.getText());
			field.addRangeFilter(valF, valT, false);
			return field.getName() + ": " + valF.toString() + " - " + valT.toString();
		} else if (eqButton.isSelected()) {
			T val = field.parseValue(eqField.getText());
			field.addEqualsFilter(val, false);
			return field.getName() + ": == " + val.toString();
		}
		return "No filter selected";
	}

}
