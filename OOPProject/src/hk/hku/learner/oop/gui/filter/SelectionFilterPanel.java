package hk.hku.learner.oop.gui.filter;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComboBox;
import javax.swing.JLabel;

import hk.hku.learner.oop.database.field.DropdownField;
import hk.hku.learner.oop.database.field.InvalidDataException;

public class SelectionFilterPanel<U extends DropdownField> extends FilterPanel<String, U>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7323482970239427412L;
	
	private JComboBox<String> jcb;

	public SelectionFilterPanel(U filterField) {
		
		super(filterField);
		
		setLayout(new GridBagLayout());
		
		jcb = new JComboBox<String>(filterField.getDropOption());
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.anchor = GridBagConstraints.WEST;
		gbc.insets = new Insets(5, 10, 5, 5);
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(new JLabel("=="), gbc);
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 0.5;
		gbc.insets = new Insets(5, 5, 5, 10);
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		add(jcb, gbc);
		
		gbc.gridy = 1;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weighty = 1.0;
		add(new JLabel(), gbc);
		
	}

	@Override
	public String applyFilter() throws InvalidDataException {
		String val = field.parseValue(jcb.getSelectedItem());
		field.addEqualsFilter(val, false);
		return field.getName() + ": == " + val;
	}
	
}
