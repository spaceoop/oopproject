package hk.hku.learner.oop.gui.filter;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import hk.hku.learner.oop.database.field.BooleanField;

public class BooleanFilterPanel extends FilterPanel<Boolean, BooleanField> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -412355995230980377L;

	JRadioButton trueField, falseField;

	public BooleanFilterPanel(BooleanField filterField) {

		super(filterField);

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		trueField = new JRadioButton("TRUE", true);
		falseField = new JRadioButton("FALSE");

		add(trueField);
		add(falseField);

		ButtonGroup bg = new ButtonGroup();
		bg.add(trueField);
		bg.add(falseField);

	}

	@Override
	public String applyFilter() {
		if (trueField.isSelected()) {
			field.addEqualsFilter(Boolean.TRUE, false);
			return field.getName() + ": == TRUE";
		} else if (falseField.isSelected()) {
			field.addEqualsFilter(Boolean.FALSE, false);
			return field.getName() + ": == FALSE";
		}
		return "No filter selected";
	}

}
