package hk.hku.learner.oop.gui.order;

import javax.swing.JTabbedPane;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.account.AccountManager;
import hk.hku.learner.oop.account.AccountType;
import hk.hku.learner.oop.food.FoodManager;
import hk.hku.learner.oop.food.OrderManager;
import hk.hku.learner.oop.gui.filter.FilterView;
import hk.hku.learner.oop.gui.stat.OrderStatistics;

public class OrderView extends JTabbedPane {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4066818803065876806L;
	
	public OrderView(Account loginAccount, OrderManager manager, AccountManager aman, FoodManager fman) {
		super(JTabbedPane.BOTTOM);
		addTab("List", new OrderList(loginAccount, manager, (event) -> setSelectedIndex(2)));
		addTab("Filter", new FilterView(manager));
		if (loginAccount.getAccountType() == AccountType.SC)
			addTab("Request", new OrderRequest(loginAccount, manager, aman, fman));
		addTab("Statistics", new OrderStatistics(manager));
	}

}
