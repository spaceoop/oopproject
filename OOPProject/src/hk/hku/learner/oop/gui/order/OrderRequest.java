package hk.hku.learner.oop.gui.order;

import java.awt.GridLayout;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.account.AccountManager;
import hk.hku.learner.oop.database.RecordNotFoundException;
import hk.hku.learner.oop.database.field.InvalidDataException;
import hk.hku.learner.oop.food.Food;
import hk.hku.learner.oop.food.FoodManager;
import hk.hku.learner.oop.food.Order;
import hk.hku.learner.oop.food.OrderManager;
import hk.hku.learner.oop.gui.utils.TextPanel;

public class OrderRequest extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6316574245356208428L;

	private AccountManager am;
	private FoodManager fm;
	private OrderManager om;
	private Account la;

	private JTextField idField;
	private JLabel nameField, descField, deliField, feeField, provField, pnameField;

	public OrderRequest(Account loginAccount, OrderManager manager, AccountManager aman, FoodManager fman) {
		la = loginAccount;
		om = manager;
		am = aman;
		fm = fman;

		// General panel layout & attribute
		setLayout(new GridLayout(1, 2));
		setBorder(new EmptyBorder(30, 30, 30, 30));

		idField = new JTextField();
		idField.addCaretListener((event) -> updateFields());

		nameField = new JLabel();
		descField = new JLabel();
		deliField = new JLabel();
		feeField = new JLabel();

		String[] fieldNames = { "Food ID", "Food Name", "Description", "Deliver Time (mins)", "Fee (HKD)" };
		JComponent[] fields = { idField, nameField, descField, deliField, feeField };
		add(new TextPanel("Request Order", fieldNames, fields));

		provField = new JLabel();
		pnameField = new JLabel();
		JButton registerButton = new JButton("Request");
		registerButton.addActionListener((event) -> registerOrder());

		String[] fieldProvNames = { "Provider ID", "Provider Name", "" };
		JComponent[] provFields = { provField, pnameField, registerButton };
		add(new TextPanel("From Provider", fieldProvNames, provFields));

	}

	private void updateFields() {

		try {
			int foodID = Integer.parseInt(idField.getText());
			Food foodRec = fm.getRecordInstance(foodID);
			nameField.setText(foodRec.getName());
			descField.setText(foodRec.getDescription());
			deliField.setText(foodRec.getDeliverTime().toString());
			feeField.setText(foodRec.getFee());

			int provID = foodRec.getProvider();
			try {
				Account provRec = am.getRecordInstance(provID);
				provField.setText(Integer.toString(provRec.getID()));
				pnameField.setText(provRec.getName());
			} catch (RecordNotFoundException e) {
				provField.setText("[Provider not found]");
				pnameField.setText("");
			}

		} catch (NumberFormatException | RecordNotFoundException e) {
			nameField.setText("[Food not found]");
			descField.setText("");
			deliField.setText("");
			feeField.setText("");
			provField.setText("");
			pnameField.setText("");
		}
	}

	private void registerOrder() {

		try {
			int foodID = Integer.parseInt(idField.getText());
			Food foodRec = fm.getRecordInstance(foodID);
			int provID = foodRec.getProvider();
			am.getRecordInstance(provID);
		} catch (NumberFormatException | RecordNotFoundException e1) {
			JOptionPane.showMessageDialog(null, "Request Fail!\nNo Food/Provider found with this ID.", "Request Fail!",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		int orderID = om.getNextAviliableID();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date date = new Date();

		String[] orderDetails = { Integer.toString(orderID), provField.getText(), Integer.toString(la.getID()),
				idField.getText(), feeField.getText(), dateFormat.format(date), "00-00-0000 00:00", "00-00-0000 00:00",
				"Requested" };

		try {
			Order myOrder = om.getInstance(orderDetails);
			om.addRecord(myOrder);
			JOptionPane.showMessageDialog(null, "Success!\nSuccessfully requested order with Food "
					+ myOrder.getFoodID() + ".\nOrder ID: " + myOrder.getID(), "Success!",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (InvalidDataException e) {
			JOptionPane.showMessageDialog(null,
					"Invalid Order!\nPlease enter the data according to the format.\nError: " + e.getMessage(),
					"Invalid Order", JOptionPane.WARNING_MESSAGE);
		}

	}

}
