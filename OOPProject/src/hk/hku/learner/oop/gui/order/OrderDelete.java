package hk.hku.learner.oop.gui.order;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.account.AccountType;
import hk.hku.learner.oop.database.RecordNotFoundException;
import hk.hku.learner.oop.food.Order;
import hk.hku.learner.oop.food.OrderManager;
import hk.hku.learner.oop.gui.utils.TextPanel;

public class OrderDelete extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1645718683489526429L;

	private static final String FRAMETITLE = "Delete Order";
	private static final int FRAMEWIDTH = 375;
	private static final int FRAMEHEIGHT = 400;

	private OrderManager om;
	private Account la;

	private JTextField idField;
	private JLabel provField, custField, foodField, feeField, reqField, stField, fiField, statField;

	public OrderDelete(Account loginAccount, OrderManager manager) {
		om = manager;
		la = loginAccount;

		// Initialize Frame
		setTitle(FRAMETITLE);
		setSize(FRAMEWIDTH, FRAMEHEIGHT);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		// Layout
		JPanel panel = new JPanel();

		GridBagConstraints gbc = new GridBagConstraints();

		// General panel layout & attribute
		panel.setLayout(new GridBagLayout());
		panel.setBorder(new EmptyBorder(30, 30, 30, 30));

		gbc.weightx = 1.0;
		gbc.gridwidth = 2;

		// Setting layout of login
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weighty = 0.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.ipady = 0;

		idField = new JTextField();
		idField.addCaretListener((event) -> updateFields());

		provField = new JLabel();
		custField = new JLabel();
		foodField = new JLabel();
		feeField = new JLabel();
		reqField = new JLabel();
		stField = new JLabel();
		fiField = new JLabel();
		statField = new JLabel();

		String[] fieldNames = { "Order ID", "Provider ID", "Customer ID", "Food ID", "Fee (HKD)", "Request Time",
				"Delivery Start Time", "Delivery Finish Time", "Delivery status" };
		JComponent[] fields = { idField, provField, custField, foodField, feeField, reqField, stField, fiField, statField };
		panel.add(new TextPanel("Delete Food", fieldNames, fields), gbc);

		// Horizontal layout of buttons
		gbc.gridwidth = 1;
		gbc.weighty = 1.0;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.SOUTH;

		gbc.gridx = 0;
		gbc.gridy = 1;

		JButton registerButton = new JButton("Delete");
		registerButton.addActionListener((event) -> deleteOrder());
		panel.add(registerButton, gbc);

		gbc.gridx = 1;

		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener((event) -> dispose());
		panel.add(cancelButton, gbc);

		add(panel);

		setVisible(true);

	}

	private void updateFields() {
		try {
			int id = Integer.parseInt(idField.getText());
			Order od = om.getRecordInstance(id);
			
			if (la.getAccountType() == AccountType.SP && od.getProviderID() != la.getID()) {
				provField.setText("[Cannot delete order");
				custField.setText("from other provider]");
				foodField.setText("");
				feeField.setText("");
				reqField.setText("");
				stField.setText("");
				fiField.setText("");
				statField.setText("");
			} else {
				provField.setText(od.getProviderID().toString());
				custField.setText(od.getCustomerID().toString());
				foodField.setText(od.getFoodID().toString());
				feeField.setText(od.getFee());
				reqField.setText(od.getRequestTime());
				stField.setText(od.getStartTime());
				fiField.setText(od.getFinishTime());
				statField.setText(od.getOrderStatus().getTypeName());
			}

		} catch (NumberFormatException | RecordNotFoundException e) {
			provField.setText("[Order not found]");
			custField.setText("");
			foodField.setText("");
			feeField.setText("");
			reqField.setText("");
			stField.setText("");
			fiField.setText("");
			statField.setText("");
		}
	}

	private void deleteOrder() {
		try {
			int id = Integer.parseInt(idField.getText());
			Order od = om.getRecordInstance(id);

			if (la.getAccountType() == AccountType.SP && od.getProviderID() != la.getID()) {
				JOptionPane.showMessageDialog(null, "Delete Fail!\nCannot delete order from other provider.", "Delete Fail!",
						JOptionPane.ERROR_MESSAGE);
				return;
			} else {
				om.removeRecord(od);
			}

			JOptionPane.showMessageDialog(null,
					"Success!\nSuccessfully deleted order with ID " + idField.getText() + ".", "Success!",
					JOptionPane.INFORMATION_MESSAGE);
			dispose();
		} catch (NumberFormatException | RecordNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Delete Fail!\nNo Record found with this ID.", "Delete Fail!",
					JOptionPane.ERROR_MESSAGE);
		}
	}

}
