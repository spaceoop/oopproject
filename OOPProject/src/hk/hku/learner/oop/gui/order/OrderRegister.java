package hk.hku.learner.oop.gui.order;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.account.AccountType;
import hk.hku.learner.oop.database.field.InvalidDataException;
import hk.hku.learner.oop.food.Order;
import hk.hku.learner.oop.food.OrderManager;
import hk.hku.learner.oop.food.OrderStatus;
import hk.hku.learner.oop.gui.utils.TextPanel;

public class OrderRegister extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6430394995195571595L;

	private static final String FRAMETITLE = "Add Order";
	private static final int FRAMEWIDTH = 375;
	private static final int FRAMEHEIGHT = 450;

	private OrderManager om;
	private Account la;

	private JTextField provField, custField, foodField, feeField, reqField, stField, fiField;
	private JComboBox<String> statField;

	public OrderRegister(Account loginAccount, OrderManager manager) {
		la = loginAccount;
		om = manager;

		// Initialize Frame
		setTitle(FRAMETITLE);
		setSize(FRAMEWIDTH, FRAMEHEIGHT);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		// Layout
		JPanel panel = new JPanel();

		GridBagConstraints gbc = new GridBagConstraints();

		// General panel layout & attribute
		panel.setLayout(new GridBagLayout());
		panel.setBorder(new EmptyBorder(30, 30, 30, 30));

		gbc.weightx = 1.0;
		gbc.gridwidth = 2;

		// Setting layout of login
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weighty = 0.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.ipady = 0;

		provField = new JTextField();
		if (la.getAccountType() == AccountType.SP) {
			provField.setEditable(false);
			provField.setText(Integer.toString(la.getID()));
		}
		custField = new JTextField();
		foodField = new JTextField();
		feeField = new JTextField();
		reqField = new JTextField();
		stField = new JTextField();
		fiField = new JTextField();
		String[] statString = { OrderStatus.REQUESTED.getTypeName(), OrderStatus.PAYED.getTypeName(),
				OrderStatus.DELIVERING.getTypeName(), OrderStatus.DELIVERED.getTypeName(),
				OrderStatus.COMPLETED.getTypeName() };
		statField = new JComboBox<String>(statString);

		String[] fieldNames = { "Provider ID", "Customer ID", "Food ID", "Fee (HKD)", "Request Time",
				"Delivery Start Time", "Delivery Finish Time", "Delivery Status" };
		JComponent[] fields = { provField, custField, foodField, feeField, reqField, stField, fiField, statField };
		panel.add(new TextPanel("Add Order", fieldNames, fields), gbc);

		// Horizontal layout of buttons
		gbc.gridwidth = 1;
		gbc.weighty = 1.0;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.SOUTH;

		gbc.gridx = 0;
		gbc.gridy = 1;

		JButton registerButton = new JButton("Add");
		registerButton.addActionListener((event) -> registerOrder());
		panel.add(registerButton, gbc);

		gbc.gridx = 1;

		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener((event) -> dispose());
		panel.add(cancelButton, gbc);

		add(panel);

		setVisible(true);
	}

	private void registerOrder() {

		int nextID = om.getNextAviliableID();

		String orderDetails[] = { Integer.toString(nextID), provField.getText(), custField.getText(),
				foodField.getText(), feeField.getText(), reqField.getText(), stField.getText(), fiField.getText(),
				(String) statField.getSelectedItem() };

		try {
			Order newOrder = new Order(orderDetails);
			om.addRecord(newOrder);
			JOptionPane.showMessageDialog(null, "Success!\nSuccessfully added order with Food " + newOrder.getFoodID()
					+ ".\nOrder ID: " + newOrder.getID(), "Success!", JOptionPane.INFORMATION_MESSAGE);
			dispose();
		} catch (InvalidDataException e) {
			JOptionPane.showMessageDialog(null,
					"Invalid Order!\nPlease enter the data according to the format.\nError: " + e.getMessage(),
					"Invalid Order", JOptionPane.WARNING_MESSAGE);
		}

	}

}
