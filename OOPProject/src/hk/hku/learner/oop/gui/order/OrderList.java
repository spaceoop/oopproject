package hk.hku.learner.oop.gui.order;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.account.AccountType;
import hk.hku.learner.oop.food.OrderManager;

public class OrderList extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3913898773486581971L;
	
	private JButton addButton;

	public OrderList(Account loginAccount, OrderManager manager, ActionListener al) {
		
		setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(20, 20, 20, 20);
		
		add(new JScrollPane(manager.getTable()), gbc);
		
		gbc.weightx = 1.0;
		gbc.weighty = 0.0;
		gbc.gridwidth = 1;
		gbc.insets = new Insets(0, 10, 10, 10);
		gbc.fill = GridBagConstraints.NONE;
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		
		if(loginAccount.getAccountType() == AccountType.SC) {
			addButton = new JButton("Request New Order");
			addButton.addActionListener(al);
			add(addButton, gbc);
		} else {
			addButton = new JButton("Add New Order");
			addButton.addActionListener((event) -> new OrderRegister(loginAccount, manager));
			add(addButton, gbc);
		}
		
		gbc.gridx = 1;
		gbc.gridy = 1;
		JButton remButton = new JButton("Remove Existing Order");
		remButton.addActionListener((event) -> new OrderDelete(loginAccount, manager));
		add(remButton, gbc);
		
	}
	
	public JButton getSwitchButton() {
		return addButton;
	}

}
