package hk.hku.learner.oop.gui.account;

import javax.swing.JTabbedPane;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.account.AccountManager;
import hk.hku.learner.oop.account.AccountType;
import hk.hku.learner.oop.gui.filter.FilterView;
import hk.hku.learner.oop.gui.stat.AccountStatistics;

public class AccountView extends JTabbedPane {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5291176056930126894L;

	public AccountView(Account loginAccount, AccountManager manager) {
		super(JTabbedPane.BOTTOM);
		addTab("My Account", new AccountMy(loginAccount, manager));
		if (loginAccount.getAccountType() == AccountType.ADM) {
			addTab("List", new AccountList(loginAccount, manager));
			addTab("Filter", new FilterView(manager));
			addTab("Statistics", new AccountStatistics(manager));
		}
	}

}
