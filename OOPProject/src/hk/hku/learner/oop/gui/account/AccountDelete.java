package hk.hku.learner.oop.gui.account;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.account.AccountManager;
import hk.hku.learner.oop.database.RecordNotFoundException;
import hk.hku.learner.oop.gui.utils.TextPanel;

public class AccountDelete extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8585578450795250034L;
	
	// Frame Init
	private static final String FRAMETITLE = "Delete Account";
	private static final int FRAMEWIDTH = 375;
	private static final int FRAMEHEIGHT = 400;
	
	private Account la;
	private AccountManager am;
	
	private JLabel acField, dobField, nameField, genderField, actypeField;
	private JTextField idField;

	public AccountDelete(Account loginAccount, AccountManager manager) {
		
		la = loginAccount;
		am = manager;
		
		// Initialize Frame
		setTitle(FRAMETITLE);
		setSize(FRAMEWIDTH, FRAMEHEIGHT);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		// Layout
		JPanel panel = new JPanel();
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		// General panel layout & attribute
		panel.setLayout(new GridBagLayout());
		panel.setBorder(new EmptyBorder(30, 30, 30, 30));
		
		gbc.weightx = 1.0;
		gbc.gridwidth = 2;
		
		// Setting layout of login
		gbc.gridx = 0; gbc.gridy = 0;
		gbc.weighty = 0.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.ipady = 0;
		
		idField = new JTextField();
		idField.addCaretListener((event) -> updateFields());
		
		acField = new JLabel();
		nameField = new JLabel();
		dobField = new JLabel();
		genderField = new JLabel();
		actypeField = new JLabel();

		String[] fieldNames = {"Account ID", "Username", "Name", "Date Of Birth", "Gender", "Account Type"};
		JComponent[] fields = {idField, acField, nameField, dobField, genderField, actypeField};
		panel.add(new TextPanel("Delete Account", fieldNames, fields), gbc);
		
		// Horizontal layout of buttons
		gbc.gridwidth = 1;
		gbc.weighty = 1.0;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.SOUTH;
		
		gbc.gridx = 0; gbc.gridy = 1;
		
		JButton registerButton = new JButton("Delete");
		registerButton.addActionListener((event) -> registerAccount());
		panel.add(registerButton, gbc);
		
		gbc.gridx = 1;
		
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener((event) -> dispose());
		panel.add(cancelButton, gbc);
		
		add(panel);
		
		setVisible(true);
	}
	
	private void updateFields() {
		try {
			int id = Integer.parseInt(idField.getText());
			Account ac = am.getRecordInstance(id);
			acField.setText(ac.getUsername());
			nameField.setText(ac.getName());
			dobField.setText(ac.getDOB());
			genderField.setText(ac.getGender());
			actypeField.setText(ac.getAccountType().getTypeName());
		} catch (NumberFormatException | RecordNotFoundException e) {
			acField.setText("[No matching account]");
			nameField.setText("");
			dobField.setText("");
			genderField.setText("");
			actypeField.setText("");
		}
		
	}
	
	private void registerAccount() {
		try {
			int delID = Integer.parseInt(idField.getText());
			if (la.getID() == delID) {
				JOptionPane.showMessageDialog(
						null, 
						"Delete Fail!\nCannot delete your own account!", 
						"Delete Fail!", 
						JOptionPane.ERROR_MESSAGE
				);
				return;
			}
			am.removeRecord(delID);
		} catch (NumberFormatException | RecordNotFoundException e) {
			JOptionPane.showMessageDialog(
					null, 
					"Delete Fail!\nNo Record found with this ID.", 
					"Delete Fail!", 
					JOptionPane.ERROR_MESSAGE
			);
			return;
		}
		JOptionPane.showMessageDialog(
				null, 
				"Success!\nSuccessfully deleted account with ID " + idField.getText() + ".", 
				"Success!", 
				JOptionPane.INFORMATION_MESSAGE
		);
		dispose();
	}
	
}
