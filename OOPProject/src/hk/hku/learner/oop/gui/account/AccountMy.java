package hk.hku.learner.oop.gui.account;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.io.File;
import java.util.Arrays;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.account.AccountManager;
import hk.hku.learner.oop.account.PasswordHasher;
import hk.hku.learner.oop.database.RecordNotFoundException;
import hk.hku.learner.oop.database.field.InvalidDataException;
import hk.hku.learner.oop.gui.utils.TextPanel;

public class AccountMy extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -491075529431624265L;

	private Account la;
	private AccountManager am;

	private JLabel acLabel;
	private JTextField nameField, dobField;
	private JRadioButton maleButton, femaleButton;

	public AccountMy(Account loginAccount, AccountManager manager) {

		la = loginAccount;
		am = manager;

		setLayout(new GridBagLayout());
		setBorder(new EmptyBorder(10, 10, 10, 10));

		GridBagConstraints gbc = new GridBagConstraints();

		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridheight = 3;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.gridx = 0;
		gbc.gridy = 0;

		acLabel = new JLabel(loginAccount.getUsername());
		nameField = new JTextField(loginAccount.getName());
		dobField = new JTextField(loginAccount.getDOB());

		maleButton = new JRadioButton("M", true);
		femaleButton = new JRadioButton("F", true);
		ButtonGroup sex = new ButtonGroup();
		sex.add(maleButton);
		sex.add(femaleButton);

		JPanel genderField = new JPanel(new GridLayout(1, 0));
		genderField.add(maleButton);
		genderField.add(femaleButton);

		JButton saveButton = new JButton("Save");
		saveButton.addActionListener((event) -> changeDetails());

		String[] detailsTitle = { "Account ID", "Username", "Name", "Date of Birth", "Gender", "Account Type", "" };
		JComponent[] detailsComponent = { new JLabel(Integer.toString(loginAccount.getID())), acLabel, nameField,
				dobField, genderField, new JLabel(loginAccount.getAccountType().getTypeName()), saveButton };

		TextPanel tp = new TextPanel("Details", detailsTitle, detailsComponent);

		add(tp, gbc);

		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridheight = 1;
		gbc.weightx = 0.0;
		gbc.weighty = 0.0;
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.insets = new Insets(20, 10, 0, 10);

		JButton acButton = new JButton("Change Username");
		acButton.addActionListener((event) -> new AccountMyUsername());
		add(acButton, gbc);

		gbc.gridy = 1;
		JButton pwButton = new JButton("Change Password");
		pwButton.addActionListener((event) -> new AccountMyPassword());
		add(pwButton, gbc);

		gbc.gridy = 2;
		gbc.weighty = 1.0;
		gbc.anchor = GridBagConstraints.SOUTH;
		gbc.insets = new Insets(0, 10, 20, 10);
		JButton delButton = new JButton("Delete Account");
		delButton.setForeground(Color.RED);
		delButton.addActionListener((event) -> deleteAccount()); 
		add(delButton, gbc);
	}
	
	private void deleteAccount() {
		int fst = JOptionPane.showConfirmDialog(null,
				"Are you sure that you want to delete your account?\nNote: Please save all records before deleting, NO record change in this session will be saved.",
				"Delete Confirmation", JOptionPane.YES_NO_OPTION);
		if (fst == JOptionPane.NO_OPTION)
			return;
		int sec = JOptionPane.showConfirmDialog(null,
				"Are you sure to delete?\nWarning: This action is IRREVERSIBLE.", "Delete Confirmation",
				JOptionPane.YES_NO_OPTION);
		if (sec == JOptionPane.NO_OPTION)
			return;

		try {
			am.removeRecord(la);
		} catch (RecordNotFoundException e) {
			System.err.println(
					"INTERNAL ERROR: This error should not be happening.\nPlease report the issue to the software developer.");
		}

		JOptionPane.showMessageDialog(null,
				"Delete Success!\nSuccessfully deleted your account.\nYou will be logged out soon.",
				"Delete Success!", JOptionPane.INFORMATION_MESSAGE);

		am.storeRecord("data" + File.separator + "account.csv");
		System.out.println("Thank you for using OOP delivery system.\nHope to see you again.");
		System.exit(0);
	}

	private void changeDetails() {
		la.setName(nameField.getText());
		la.setDOB(dobField.getText());
		if (maleButton.isSelected()) {
			la.setGender("M");
		} else {
			la.setGender("F");
		}
		try {
			am.updateRecord(la);
			JOptionPane.showMessageDialog(null, "Success!\nSuccessfully updated account details.", "Success!",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (InvalidDataException e) {
			JOptionPane.showMessageDialog(null,
					"Register Fail!\nPlease enter the data according to the format.\nError: " + e.getMessage(),
					"Register Fail!", JOptionPane.WARNING_MESSAGE);
		} catch (RecordNotFoundException e) {
			System.err.println(
					"INTERNAL ERROR: This error should not be happening.\nPlease report the issue to the software developer.");
		}
	}

	private class AccountMyUsername extends JFrame {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4920652046622699928L;

		private static final String FRAMETITLE = "Change Username";
		private static final int FRAMEWIDTH = 375;
		private static final int FRAMEHEIGHT = 300;

		private JLabel oacField;
		private JTextField nacField;
		private JPasswordField pwField;

		public AccountMyUsername() {

			// Initialize Frame
			setTitle(FRAMETITLE);
			setSize(FRAMEWIDTH, FRAMEHEIGHT);
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);

			// Layout
			JPanel panel = new JPanel();

			GridBagConstraints gbc = new GridBagConstraints();

			// General panel layout & attribute
			panel.setLayout(new GridBagLayout());
			panel.setBorder(new EmptyBorder(30, 30, 30, 30));

			gbc.weightx = 1.0;
			gbc.gridwidth = 2;

			// Setting layout of login
			gbc.gridx = 0;
			gbc.gridy = 0;
			gbc.weighty = 0.0;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.ipady = 0;

			oacField = new JLabel(la.getUsername());
			nacField = new JTextField();
			pwField = new JPasswordField();

			String[] fieldNames = { "Old username", "New username", "Confirm with password" };
			JComponent[] fields = { oacField, nacField, pwField };
			panel.add(new TextPanel("Change username", fieldNames, fields), gbc);

			// Horizontal layout of buttons
			gbc.gridwidth = 1;
			gbc.weighty = 1.0;
			gbc.fill = GridBagConstraints.NONE;
			gbc.anchor = GridBagConstraints.SOUTH;

			gbc.gridx = 0;
			gbc.gridy = 1;

			JButton registerButton = new JButton("Change");
			registerButton.addActionListener((event) -> changeUsername());
			panel.add(registerButton, gbc);

			gbc.gridx = 1;

			JButton cancelButton = new JButton("Cancel");
			cancelButton.addActionListener((event) -> dispose());
			panel.add(cancelButton, gbc);

			add(panel);

			setVisible(true);

		}

		private void changeUsername() {
			String newACName = nacField.getText();
			try {
				am.findAccount(newACName);
				JOptionPane.showMessageDialog(null,
						"Invalid Username!\nUsername " + newACName + " has been registed before.", "Invalid Username!",
						JOptionPane.WARNING_MESSAGE);
				return;
			} catch (RecordNotFoundException e) {

			}

			if (!PasswordHasher.getHashedPassword(pwField.getPassword(), la.getSalt()).equals(la.getHashedPassword())) {
				JOptionPane.showMessageDialog(null, "Invalid Password!\nPlease reenter a correct password.",
						"Invalid Password!", JOptionPane.WARNING_MESSAGE);
				return;
			}

			la.setUsername(newACName);

			try {
				am.updateRecord(la);
			} catch (InvalidDataException | RecordNotFoundException e) {
				System.err.println(
						"INTERNAL ERROR: This error should not be happening.\nPlease report the issue to the software developer.");
			}

			JOptionPane.showMessageDialog(null, "Success!\nSuccessfully changed username to " + newACName + ".",
					"Success!", JOptionPane.INFORMATION_MESSAGE);

			oacField.setText(newACName);

			dispose();

		}

	}

	private class AccountMyPassword extends JFrame {

		/**
		 * 
		 */
		private static final long serialVersionUID = -7837502333565603227L;

		private static final String FRAMETITLE = "Change Username";
		private static final int FRAMEWIDTH = 375;
		private static final int FRAMEHEIGHT = 300;

		private JPasswordField opwField, npwField, rpwField;

		public AccountMyPassword() {

			// Initialize Frame
			setTitle(FRAMETITLE);
			setSize(FRAMEWIDTH, FRAMEHEIGHT);
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);

			// Layout
			JPanel panel = new JPanel();

			GridBagConstraints gbc = new GridBagConstraints();

			// General panel layout & attribute
			panel.setLayout(new GridBagLayout());
			panel.setBorder(new EmptyBorder(30, 30, 30, 30));

			gbc.weightx = 1.0;
			gbc.gridwidth = 2;

			// Setting layout of login
			gbc.gridx = 0;
			gbc.gridy = 0;
			gbc.weighty = 0.0;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.ipady = 0;

			opwField = new JPasswordField();
			npwField = new JPasswordField();
			rpwField = new JPasswordField();

			String[] fieldNames = { "Old password", "New password", "Confirm new password" };
			JComponent[] fields = { opwField, npwField, rpwField };
			panel.add(new TextPanel("Change password", fieldNames, fields), gbc);

			// Horizontal layout of buttons
			gbc.gridwidth = 1;
			gbc.weighty = 1.0;
			gbc.fill = GridBagConstraints.NONE;
			gbc.anchor = GridBagConstraints.SOUTH;

			gbc.gridx = 0;
			gbc.gridy = 1;

			JButton registerButton = new JButton("Change");
			registerButton.addActionListener((event) -> changePassword());
			panel.add(registerButton, gbc);

			gbc.gridx = 1;

			JButton cancelButton = new JButton("Cancel");
			cancelButton.addActionListener((event) -> dispose());
			panel.add(cancelButton, gbc);

			add(panel);

			setVisible(true);

		}

		private void changePassword() {

			if (!PasswordHasher.getHashedPassword(opwField.getPassword(), la.getSalt())
					.equals(la.getHashedPassword())) {
				JOptionPane.showMessageDialog(null, "Incorrect old password!\nPlease reenter the correct old password.",
						"Invalid Password!", JOptionPane.WARNING_MESSAGE);
				return;
			}

			if (!Arrays.equals(npwField.getPassword(), rpwField.getPassword())) {
				JOptionPane.showMessageDialog(null,
						"Password Change Fail!\nPassword Retype does not match the password.", "Password Change Fail!",
						JOptionPane.WARNING_MESSAGE);
				return;
			}

			String newHashedPassword = PasswordHasher.getHashedPassword(npwField.getPassword(), la.getSalt());
			la.setHashedPassword(newHashedPassword);

			try {
				am.updateRecord(la);
			} catch (InvalidDataException | RecordNotFoundException e) {
				System.err.println(
						"INTERNAL ERROR: This error should not be happening.\nPlease report the issue to the software developer.");
			}

			JOptionPane.showMessageDialog(null, "Success!\nSuccessfully changed password.", "Success!",
					JOptionPane.INFORMATION_MESSAGE);

			dispose();

		}

	}
}
