package hk.hku.learner.oop.gui.stat;

import hk.hku.learner.oop.food.FoodManager;

public class FoodStatistics extends StatisticsView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5299302423081120827L;

	public FoodStatistics(FoodManager manager) {
		super(manager);
		
		double priceSum = 0.0;
		for(int i=0;i<manager.getTable().getModel().getRowCount();i++)
			priceSum += Double.valueOf((String) manager.getTable().getModel().getValueAt(i, 5)); 
		
		int delivTime = 0;
		for(int i=0;i<manager.getTable().getModel().getRowCount();i++)
			delivTime += (Integer) manager.getTable().getModel().getValueAt(i, 4);
		
		if (manager.getTable().getModel().getRowCount() > 0) {
			addStat("Average Food Price", String.format("%.2f", priceSum / manager.getTable().getModel().getRowCount()));
			addStat("Average Delivery Time", String.format("%.2f", 1.0 * delivTime / manager.getTable().getModel().getRowCount()));
		}
		
	}

}
