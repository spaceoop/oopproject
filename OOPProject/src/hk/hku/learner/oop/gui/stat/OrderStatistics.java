package hk.hku.learner.oop.gui.stat;

import java.util.EnumMap;
import java.util.Map;

import hk.hku.learner.oop.food.OrderManager;
import hk.hku.learner.oop.food.OrderStatus;

public class OrderStatistics extends StatisticsView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5650620376394580908L;

	public OrderStatistics(OrderManager manager) {
		super(manager);
		
		double priceSum = 0.0;
		for(int i=0;i<manager.getTable().getModel().getRowCount();i++)
			priceSum += Double.valueOf((String) manager.getTable().getModel().getValueAt(i, 4)); 
		
		OrderStatus os[] = {OrderStatus.REQUESTED, OrderStatus.PAYED, OrderStatus.DELIVERING, OrderStatus.DELIVERED, OrderStatus.COMPLETED};
		
		Map<OrderStatus, Integer> count = new EnumMap<OrderStatus, Integer>(OrderStatus.class);
		for(int i=0;i<5;i++) count.put(os[i], 0);
		
		for(int i=0;i<manager.getTable().getModel().getRowCount();i++) {
			OrderStatus myType = OrderStatus.getStatus((String) manager.getTable().getModel().getValueAt(i, 8));
			Integer foo = count.get(myType) + 1;
			count.put(myType, foo);
		}
		
		for(int i=0;i<5;i++) {
			addStat(os[i].getTypeName() + " Record", count.get(os[i]).toString());
		}
		
		if (manager.getTable().getModel().getRowCount() > 0) 
			addStat("Average Food Price", String.format("%.2f", priceSum / manager.getTable().getModel().getRowCount()));
		
	}

}
