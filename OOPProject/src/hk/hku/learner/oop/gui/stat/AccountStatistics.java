package hk.hku.learner.oop.gui.stat;

import hk.hku.learner.oop.account.AccountManager;

public class AccountStatistics extends StatisticsView {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3132585178459707699L;

	public AccountStatistics(AccountManager manager) {
		super(manager);
		
		// 6
		int lo = 0, hi = 0;
		
		for(int i=0;i<manager.getTable().getModel().getRowCount();i++)
			if (manager.getTable().getModel().getValueAt(i, 6).equals("M"))
				lo++;
			else
				hi++;
		
		addStat("Number of Male", Integer.toString(lo));
		addStat("Number of Female", Integer.toString(hi));
		
	}

}
