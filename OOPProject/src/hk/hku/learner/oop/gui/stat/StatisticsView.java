package hk.hku.learner.oop.gui.stat;

import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import hk.hku.learner.oop.database.DataManager;
import hk.hku.learner.oop.gui.utils.TextPanel;

public class StatisticsView extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4068664678170939192L;

	private DataManager<?> dm;
	private StatisticsPanel sp;

	public StatisticsView(DataManager<?> manager) {
		dm = manager;
		sp = new StatisticsPanel();

		setLayout(new GridLayout(0, 1));
		
		addStat("Total No. of Records", Integer.toString(dm.getTable().getModel().getRowCount()));
		
		add(sp);
	}

	public void addStat(String fieldName, String field) {
		JTextField jtf = new JTextField(field);
		jtf.setEditable(false);
		sp.addStat(fieldName, jtf);
	}

	private class StatisticsPanel extends TextPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 5861365574681131690L;

		private int i;
		private GridBagConstraints gbc;

		public StatisticsPanel() {
			super("Statistics");
			gbc = new GridBagConstraints();
			gbc.weighty = 1.0;
			gbc.gridx = 0;
			gbc.gridy = 10000;
			add(new JLabel(), gbc);
			
			gbc.anchor = GridBagConstraints.EAST;
			gbc.insets = new Insets(5, 5, 5, 5);
			gbc.weighty = 0.0;
			i = 0;
		}

		public void addStat(String fieldName, JComponent field) {

			gbc.fill = GridBagConstraints.NONE;
			gbc.gridy = i;
			gbc.gridx = 0;
			gbc.weightx = 0.0;
			add(new JLabel(fieldName), gbc);

			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.gridy = i;
			gbc.gridx = 1;
			gbc.weightx = 0.8;
			add(field, gbc);

			i++;

		}

	}

}
