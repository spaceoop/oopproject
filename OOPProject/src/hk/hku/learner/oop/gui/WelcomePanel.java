package hk.hku.learner.oop.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import hk.hku.learner.oop.account.Account;

public class WelcomePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6606277094678592831L;

	private Account la;

	public WelcomePanel(Account loginAccount) {

		la = loginAccount;

		setLayout(new BorderLayout());
		setBackground(Color.WHITE);

		final String spacePath = "assets" + File.separator + "space.png";
		File spaceImgFile = new File(spacePath);
		try {
			Image spaceImg = ImageIO.read(spaceImgFile).getScaledInstance(180, 50, Image.SCALE_SMOOTH);
			JLabel spaceImgLabel = new JLabel(new ImageIcon(spaceImg), SwingConstants.RIGHT);
			add(spaceImgLabel, BorderLayout.NORTH);
		} catch (IOException e) {
			System.err.println("ERROR: Cannot load image");
		}

		final String logoPath = "assets" + File.separator + "logo.png";
		File logoImgFile = new File(logoPath);
		try {
			Image logoImg = ImageIO.read(logoImgFile).getScaledInstance(400, 200, Image.SCALE_SMOOTH);
			JLabel logoImgLabel = new JLabel(new ImageIcon(logoImg), SwingConstants.CENTER);
			add(logoImgLabel, BorderLayout.CENTER);
		} catch (IOException e) {
			System.err.println("ERROR: Cannot load image");
		}

		add(new WelcomeInfo(), BorderLayout.SOUTH);

	}

	private class WelcomeInfo extends JPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = -7548251369850822134L;

		public WelcomeInfo() {
			setLayout(new GridLayout(1, 2));
			setBackground(Color.WHITE);
			
			setBorder(new EmptyBorder(20, 0, 0, 0));
			
			JLabel leftLabel = new JLabel("System developed by 17-18 Sem 1 3G4.", SwingConstants.LEFT);
			add(leftLabel);
			JLabel rightLabel = new JLabel("Your Account: " + la.getName() +" [" + la.getUsername() + " - " + la.getID() + "]", SwingConstants.RIGHT);
			add(rightLabel);
		}

	}

}
