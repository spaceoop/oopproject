package hk.hku.learner.oop.gui.login;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.io.File;
import java.util.Arrays;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.account.AccountManager;
import hk.hku.learner.oop.account.AccountType;
import hk.hku.learner.oop.account.PasswordHasher;
import hk.hku.learner.oop.database.RecordNotFoundException;
import hk.hku.learner.oop.database.field.InvalidDataException;
import hk.hku.learner.oop.gui.utils.TextPanel;

public class RegisterWindow extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -884718592287647987L;

	// Frame Init
	private static final String FRAMETITLE = "Registration";
	private static final int FRAMEWIDTH = 375;
	private static final int FRAMEHEIGHT = 450;

	private JTextField acField, dobField, nameField;
	private JPasswordField pwField, repwField;
	private JRadioButton maleButton, femaleButton;
	private JComboBox<String> actypeField;

	public RegisterWindow() {
		// Initialize Frame
		setTitle(FRAMETITLE);
		setSize(FRAMEWIDTH, FRAMEHEIGHT);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		// Layout
		JPanel panel = new JPanel();

		GridBagConstraints gbc = new GridBagConstraints();

		// General panel layout & attribute
		panel.setLayout(new GridBagLayout());
		panel.setBorder(new EmptyBorder(30, 30, 30, 30));

		gbc.weightx = 1.0;
		gbc.gridwidth = 2;

		// Setting layout of login
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weighty = 0.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.ipady = 0;

		acField = new JTextField();
		pwField = new JPasswordField();
		repwField = new JPasswordField();
		dobField = new JTextField();
		nameField = new JTextField();

		maleButton = new JRadioButton("M", true);
		femaleButton = new JRadioButton("F", true);
		ButtonGroup sex = new ButtonGroup();
		sex.add(maleButton);
		sex.add(femaleButton);

		String[] acType = { AccountType.SP.getTypeName(), AccountType.SC.getTypeName() };
		actypeField = new JComboBox<String>(acType);

		JPanel genderField = new JPanel(new GridLayout(1, 0));
		genderField.add(maleButton);
		genderField.add(femaleButton);

		String[] fieldNames = { "Username", "Password", "Retype Password", "Name", "Date Of Birth", "Gender",
				"Account Type" };
		JComponent[] fields = { acField, pwField, repwField, nameField, dobField, genderField, actypeField };
		panel.add(new TextPanel("Registration", fieldNames, fields), gbc);

		// Horizontal layout of buttons
		gbc.gridwidth = 1;
		gbc.weighty = 1.0;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.SOUTH;

		gbc.gridx = 0;
		gbc.gridy = 1;

		JButton registerButton = new JButton("Register");
		registerButton.addActionListener((event) -> registerAccount());
		panel.add(registerButton, gbc);

		gbc.gridx = 1;

		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener((event) -> dispose());
		panel.add(cancelButton, gbc);

		add(panel);

		setVisible(true);
	}

	private void registerAccount() {

		if (!Arrays.equals(pwField.getPassword(), repwField.getPassword())) {
			JOptionPane.showMessageDialog(null, "Register Fail!\nPassword Retype does not match the password.",
					"Register Fail!", JOptionPane.WARNING_MESSAGE);
			return;
		}

		Account loginManager = AccountManager.getLoginManager();
		AccountManager accMan = new AccountManager(loginManager);

		accMan.loadRecord("data" + File.separator + "account.csv");

		try {
			accMan.findAccount(acField.getText());
			JOptionPane.showMessageDialog(null,
					"Register Fail!\nUsername " + acField.getText() + " has been registed before.", "Register Fail!",
					JOptionPane.WARNING_MESSAGE);
			return;
		} catch (RecordNotFoundException e1) {
		}

		int id = accMan.getNextAviliableID();
		String salt = PasswordHasher.getNextSalt();

		String[] newAcDetails = { Integer.toString(id), acField.getText(), salt,
				PasswordHasher.getHashedPassword(pwField.getPassword(), salt), nameField.getText(), dobField.getText(),
				maleButton.isSelected() ? "M" : "F", "false", (String) actypeField.getSelectedItem() };

		try {
			Account newAccount = new Account(newAcDetails);
			accMan.addRecord(newAccount);
		} catch (InvalidDataException e) {
			JOptionPane.showMessageDialog(null,
					"Register Fail!\nPlease enter the data according to the format.\nError: " + e.getMessage(),
					"Register Fail!", JOptionPane.WARNING_MESSAGE);
			return;
		}

		accMan.storeRecord("data" + File.separator + "account.csv");

		JOptionPane.showMessageDialog(null,
				"Register Success!\nSuccessfully registered account " + acField.getText()
						+ ".\nPlease wait for approval by administrator.",
				"Register Success!", JOptionPane.INFORMATION_MESSAGE);

		dispose();

	}

}