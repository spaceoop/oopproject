package hk.hku.learner.oop.gui.login;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.account.AccountManager;
import hk.hku.learner.oop.account.PasswordHasher;
import hk.hku.learner.oop.database.RecordNotFoundException;
import hk.hku.learner.oop.gui.MainFrame;
import hk.hku.learner.oop.gui.utils.TextPanel;

public class LoginWindow extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = -884718592287647987L;
	
	// Frame Init
	private static final String FRAMETITLE = "Login";
	private static final int FRAMEWIDTH = 375;
	private static int FRAMEHEIGHT = 300;
	
	private JTextField acField;
	private JPasswordField pwField;
	
	public LoginWindow() {
		// Initialize Frame
		setTitle(FRAMETITLE);
		setSize(FRAMEWIDTH, FRAMEHEIGHT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.out.println("Thank you for using OOP delivery system.\nHope to see you again.");
			}
		});

		// Layout
		JPanel panel = new JPanel();
		GridBagConstraints gbc = new GridBagConstraints();

		// General panel layout & attribute
		panel.setLayout(new GridBagLayout());
		panel.setBorder(new EmptyBorder(30, 30, 30, 30));
		
		gbc.weightx = 1.0; gbc.gridwidth = 2;
		gbc.gridx = 0; gbc.gridy = 0;
		gbc.insets = new Insets(0, 0, 20, 0);
		JLabel welcomeLabel = new JLabel("Welcome to OOP Delivery!");
		welcomeLabel.setFont(welcomeLabel.getFont().deriveFont(20.0f));
		panel.add(welcomeLabel, gbc);
		
		// Setting layout of login
		gbc.gridy = 1; gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(0, 0, 0, 0);
		
		acField = new JTextField();
		pwField = new JPasswordField();
		pwField.addActionListener((event) -> loginAccount());
		
		String[] fieldNames = {"Username", "Password"};
		JComponent[] fields = {acField, pwField};
		panel.add(new TextPanel("Login", fieldNames, fields), gbc);
		
		// Horizontal layout of buttons
		gbc.gridy = 2; gbc.gridwidth = 1; gbc.fill = GridBagConstraints.NONE;
		gbc.weighty = 1.0; gbc.anchor = GridBagConstraints.SOUTH;
		
		gbc.gridx = 0;
		JButton loginButton = new JButton("Log In");
		loginButton.addActionListener((event) -> loginAccount());
		panel.add(loginButton, gbc);
		
		gbc.gridx = 1;
		JButton registerButton = new JButton("Register");
		registerButton.addActionListener((event) -> new RegisterWindow());
		panel.add(registerButton, gbc);
		
		add(panel);
		setVisible(true);
	}
	
	private void loginAccount() {
		
		Account loginManager = AccountManager.getLoginManager();
		AccountManager accMan = new AccountManager(loginManager);
		
		accMan.loadRecord("data" + File.separator + "account.csv");
		
		try {
			Account ac = accMan.findAccount(acField.getText());
			if(!PasswordHasher.getHashedPassword(pwField.getPassword(), ac.getSalt()).equals(ac.getHashedPassword())) {
				JOptionPane.showMessageDialog(
						null, 
						"Login Fail!\nIncorrect password.", 
						"Login Fail!", 
						JOptionPane.WARNING_MESSAGE
				);
				return;
			}
			
			if(!ac.getApproved()) {
				JOptionPane.showMessageDialog(
						null, 
						"Login Fail!\nAccount not approved by administrator.", 
						"Login Fail!", 
						JOptionPane.WARNING_MESSAGE
				);
				return;
			}
			
			JOptionPane.showMessageDialog(
					null, 
					"Login Success!\nSuccessfully logged in as " + ac.getUsername() + ".", 
					"Login Success!", 
					JOptionPane.INFORMATION_MESSAGE
			);
			
			new MainFrame(ac);
			dispose();
			
		} catch (RecordNotFoundException e) {
			JOptionPane.showMessageDialog(
					null, 
					"Login Fail!\nAccount not found.", 
					"Login Fail!", 
					JOptionPane.WARNING_MESSAGE
			);
		}
		
	}

}