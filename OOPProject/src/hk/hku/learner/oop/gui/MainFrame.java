package hk.hku.learner.oop.gui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.account.AccountManager;
import hk.hku.learner.oop.account.AccountType;
import hk.hku.learner.oop.food.FoodManager;
import hk.hku.learner.oop.food.OrderManager;
import hk.hku.learner.oop.gui.account.AccountView;
import hk.hku.learner.oop.gui.food.FoodView;
import hk.hku.learner.oop.gui.login.LoginWindow;
import hk.hku.learner.oop.gui.order.OrderView;

public class MainFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7177669228050795504L;

	private static final PanelType[] panelType = { PanelType.DASH, PanelType.ACM, PanelType.FOODM, PanelType.ORDERM };

	private Account ac;
	private AccountManager am;
	private FoodManager fm;
	private OrderManager om;

	public MainFrame(Account loginAccount) {
		super("OOP Food Delivery System");
		setSize(800, 450);

		JPanel cardsPanel = new JPanel(new CardLayout());

		ac = loginAccount;
		am = new AccountManager(loginAccount);
		fm = new FoodManager(loginAccount);
		om = new OrderManager(loginAccount);

		am.loadRecord("data" + File.separator + "account.csv");
		fm.loadRecord("data" + File.separator + "food.csv");
		om.loadRecord("data" + File.separator + "order.csv");

		final Component[] cards = { new WelcomePanel(loginAccount), new AccountView(loginAccount, am), new FoodView(loginAccount, fm),
				new OrderView(loginAccount, om, am, fm) };

		for (int i = 0; i < cards.length; i++)
			cardsPanel.add(cards[i], panelType[i].getIdentifier());

		Container conPane = getContentPane();
		conPane.add(cardsPanel, BorderLayout.CENTER);
		conPane.add(new ButtonPanel(cardsPanel), BorderLayout.LINE_START);

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				logoutAccount();
			}
		});
		setVisible(true);

	}

	private void logoutAccount() {

		int choice = JOptionPane.showConfirmDialog(null, "Do you want to save all records before logout?", "Save",
				JOptionPane.YES_NO_CANCEL_OPTION);

		if (choice == JOptionPane.YES_OPTION) {
			am.storeRecord("data" + File.separator + "account.csv");
			fm.storeRecord("data" + File.separator + "food.csv");
			om.storeRecord("data" + File.separator + "order.csv");

			JOptionPane.showMessageDialog(null, "Logout Success!\nSuccessfully saved all records and logged out.",
					"Logout Success!", JOptionPane.INFORMATION_MESSAGE);

			SwingUtilities.invokeLater(() -> new LoginWindow());
			dispose();

		} else if (choice == JOptionPane.NO_OPTION) {
			JOptionPane.showMessageDialog(null, "Logout Success!\nSuccessfully logged out.", "Logout Success!",
					JOptionPane.INFORMATION_MESSAGE);

			SwingUtilities.invokeLater(() -> new LoginWindow());
			dispose();
		}

	}

	private enum PanelType {
		DASH("DASHBOARD", "Dashboard", "Dashboard", "Dashboard"), ACM("ACCOUNT", "Account Management", "My Account",
				"My Account"), FOODM("FOOD", "Food Management", "Food Management",
						"Food Menu"), ORDERM("ORDER", "Order Management", "Order Management", "My Order");

		private String identifier, admLabel, spLabel, scLabel;

		private PanelType(String pid, String amdl, String spl, String scl) {
			identifier = pid;
			admLabel = amdl;
			spLabel = spl;
			scLabel = scl;
		}

		public String getIdentifier() {
			return identifier;
		}

		public String getLabel(AccountType ac) {
			if (ac == AccountType.ADM)
				return admLabel;
			else if (ac == AccountType.SC)
				return scLabel;
			else
				return spLabel;
		}
	}

	private class ButtonPanel extends JPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = -6391284200951765906L;

		public ButtonPanel(JPanel parent) {
			setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 0;

			JButton[] buttons = new JButton[panelType.length];
			for (int i = 0; i < panelType.length; i++) {
				buttons[i] = new JButton(panelType[i].getLabel(ac.getAccountType()));
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.gridy = i;
				gbc.anchor = GridBagConstraints.NORTHWEST;
				buttons[i].addActionListener(new CardChangeListener(parent, panelType[i].getIdentifier()));
				add(buttons[i], gbc);
			}

			gbc.anchor = GridBagConstraints.SOUTHWEST;
			gbc.gridy++;
			gbc.weighty = 1.0;
			add(new JLabel(), gbc);
			gbc.gridy++;
			gbc.weighty = 0.0;
			gbc.insets = new Insets(8, 8, 8, 8);
			add(new LogoutPanel(), gbc);
		}

		private class LogoutPanel extends JPanel {

			/**
			 * 
			 */
			private static final long serialVersionUID = -60194538851227600L;

			public LogoutPanel() {
				setLayout(new GridBagLayout());
				GridBagConstraints gbd = new GridBagConstraints();

				gbd.gridx = 0;
				gbd.gridy = 0;
				add(new JLabel("Current Logged in: "), gbd);
				gbd.gridy++;
				add(new JLabel(ac.getName()), gbd);
				gbd.gridy++;
				JButton logButton = new JButton("Logout");
				logButton.addActionListener((event) -> logoutAccount());
				add(logButton, gbd);
				setBorder(new CompoundBorder(new LineBorder(Color.GRAY, 3, true), new EmptyBorder(5, 5, 5, 5)));
			}

		}

		private class CardChangeListener implements ActionListener {

			String cardName;
			JPanel cardPanel;

			public CardChangeListener(JPanel cardPane, String cardString) {
				cardPanel = cardPane;
				cardName = cardString;
			}

			@Override
			public void actionPerformed(ActionEvent e) {
				CardLayout cl = (CardLayout) cardPanel.getLayout();
				cl.show(cardPanel, cardName);
			}

		}

	}

}
