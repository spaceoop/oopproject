package hk.hku.learner.oop.gui.utils;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

public class TextPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2131438731925297593L;
	
	protected TextPanel(String title) {
		setLayout(new GridBagLayout());
		
		// Border setting
		TitledBorder border = new TitledBorder(title);
		border.setTitleJustification(TitledBorder.CENTER);
		border.setTitleColor(Color.BLACK);
		border.setTitleFont(border.getTitleFont().deriveFont(Font.BOLD));
		setBorder(new CompoundBorder(border, new EmptyBorder(10, 10, 10, 10)));
	}
	
	public TextPanel(String title, String[] fieldName, JComponent[] fields) {
		
		this(title);		
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.EAST;
		gbc.insets = new Insets(5, 5, 5, 5);
		
		for(int i = 0; i < fieldName.length; i++) {
			
			gbc.fill = GridBagConstraints.NONE;
			gbc.gridy = i;
			gbc.gridx = 0;
			gbc.weightx = 0.0;
			add(new JLabel(fieldName[i]) , gbc);
			
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.gridy = i;
			gbc.gridx = 1;
			gbc.weightx = 0.8;	
			add(fields[i], gbc);
			
		}
	}
	
	public TextPanel(String title, JComponent[] fields) {
		
		this(title);
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.EAST;
		gbc.insets = new Insets(5, 5, 5, 5);
		
		for(int i = 0; i < fields.length; i++) {
			
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.gridy = i;
			gbc.gridx = 0;
			gbc.weightx = 0.8;	
			add(fields[i], gbc);
			
		}
	}

}