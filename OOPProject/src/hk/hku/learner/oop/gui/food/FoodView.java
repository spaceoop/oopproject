package hk.hku.learner.oop.gui.food;

import javax.swing.JTabbedPane;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.food.FoodManager;
import hk.hku.learner.oop.gui.filter.FilterView;
import hk.hku.learner.oop.gui.stat.FoodStatistics;

public class FoodView extends JTabbedPane {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3942090299913315632L;
	
	public FoodView(Account loginAccount, FoodManager manager) {
		super(JTabbedPane.BOTTOM);
		addTab("List", new FoodList(loginAccount, manager));
		addTab("Filter", new FilterView(manager));
		addTab("Statistics", new FoodStatistics(manager));
	}

}
