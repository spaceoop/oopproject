package hk.hku.learner.oop.gui.food;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.account.AccountType;
import hk.hku.learner.oop.database.RecordNotFoundException;
import hk.hku.learner.oop.food.Food;
import hk.hku.learner.oop.food.FoodManager;
import hk.hku.learner.oop.gui.utils.TextPanel;

public class FoodDelete extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3913300878751315212L;

	private static final String FRAMETITLE = "Delete Food";
	private static final int FRAMEWIDTH = 375;
	private static final int FRAMEHEIGHT = 400;

	private Account la;
	private FoodManager fm;

	private JTextField idField;
	private JLabel provField, nameField, descField, deliField, feeField;

	public FoodDelete(Account loginAccount, FoodManager manager) {

		la = loginAccount;
		fm = manager;

		// Initialize Frame
		setTitle(FRAMETITLE);
		setSize(FRAMEWIDTH, FRAMEHEIGHT);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		// Layout
		JPanel panel = new JPanel();

		GridBagConstraints gbc = new GridBagConstraints();

		// General panel layout & attribute
		panel.setLayout(new GridBagLayout());
		panel.setBorder(new EmptyBorder(30, 30, 30, 30));

		gbc.weightx = 1.0;
		gbc.gridwidth = 2;

		// Setting layout of login
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weighty = 0.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.ipady = 0;

		idField = new JTextField();
		idField.addCaretListener((event) -> updateFields());

		provField = new JLabel();
		nameField = new JLabel();
		descField = new JLabel();
		deliField = new JLabel();
		feeField = new JLabel();

		String[] fieldNames = { "Food ID", "Provider ID", "Name", "Description", "Deliver Time", "Fee (HKD)" };
		JComponent[] fields = { idField, provField, nameField, descField, deliField, feeField };
		panel.add(new TextPanel("Delete Food", fieldNames, fields), gbc);

		// Horizontal layout of buttons
		gbc.gridwidth = 1;
		gbc.weighty = 1.0;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.SOUTH;

		gbc.gridx = 0;
		gbc.gridy = 1;

		JButton registerButton = new JButton("Delete");
		registerButton.addActionListener((event) -> registerFood());
		panel.add(registerButton, gbc);

		gbc.gridx = 1;

		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener((event) -> dispose());
		panel.add(cancelButton, gbc);

		add(panel);

		setVisible(true);

	}

	private void registerFood() {
		try {
			int id = Integer.parseInt(idField.getText());
			Food fd = fm.getRecordInstance(id);

			if (fd.getProvider() != la.getID() && la.getAccountType() == AccountType.SP) {
				JOptionPane.showMessageDialog(null, "Delete Fail!\nCannot delete food from other provider.", "Delete Fail!",
						JOptionPane.ERROR_MESSAGE);
				return;
			} else {
				fm.removeRecord(fd);
			}

			JOptionPane.showMessageDialog(null,
					"Success!\nSuccessfully deleted food with ID " + idField.getText() + ".", "Success!",
					JOptionPane.INFORMATION_MESSAGE);
			dispose();

		} catch (NumberFormatException | RecordNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Delete Fail!\nNo Record found with this ID.", "Delete Fail!",
					JOptionPane.ERROR_MESSAGE);
		}

	}

	private void updateFields() {
		try {
			int id = Integer.parseInt(idField.getText());
			Food fd = fm.getRecordInstance(id);

			if (fd.getProvider() != la.getID() && la.getAccountType() == AccountType.SP) {
				provField.setText("[Cannot delete food");
				nameField.setText("from other provider]");
				descField.setText("");
				deliField.setText("");
				feeField.setText("");
			} else {
				provField.setText(fd.getProvider().toString());
				nameField.setText(fd.getName());
				descField.setText(fd.getDescription());
				deliField.setText(fd.getDeliverTime().toString());
				feeField.setText(fd.getFee());
			}

		} catch (NumberFormatException | RecordNotFoundException e) {
			provField.setText("[Food not found]");
			nameField.setText("");
			descField.setText("");
			deliField.setText("");
			feeField.setText("");
		}
	}
}
