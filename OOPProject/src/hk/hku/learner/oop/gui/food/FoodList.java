package hk.hku.learner.oop.gui.food;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.account.AccountType;
import hk.hku.learner.oop.food.FoodManager;

public class FoodList extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6769858641084396346L;
	
	public FoodList(Account loginAccount, FoodManager manager) {
		
		setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(20, 20, 20, 20);
		
		add(new JScrollPane(manager.getTable()), gbc);
		
		if(loginAccount.getAccountType() != AccountType.SC) {
			gbc.weightx = 1.0;
			gbc.weighty = 0.0;
			gbc.gridwidth = 1;
			gbc.insets = new Insets(0, 10, 10, 10);
			gbc.fill = GridBagConstraints.NONE;
			
			gbc.gridx = 0;
			gbc.gridy = 1;
			JButton addButton = new JButton("Add New Food");
			addButton.addActionListener((event) -> new FoodRegister(loginAccount, manager));
			add(addButton, gbc);
			
			gbc.gridx = 1;
			gbc.gridy = 1;
			JButton remButton = new JButton("Remove Existing Food");
			remButton.addActionListener((event) -> new FoodDelete(loginAccount, manager));
			add(remButton, gbc);
		}
		
	}

}
