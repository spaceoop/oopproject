package hk.hku.learner.oop.gui.food;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import hk.hku.learner.oop.account.Account;
import hk.hku.learner.oop.account.AccountType;
import hk.hku.learner.oop.database.field.InvalidDataException;
import hk.hku.learner.oop.food.Food;
import hk.hku.learner.oop.food.FoodManager;
import hk.hku.learner.oop.gui.utils.TextPanel;

public class FoodRegister extends JFrame {

	/**
	* 
	*/
	private static final long serialVersionUID = 282158971821752959L;
	// Frame Init
	private static final String FRAMETITLE = "Add Account";
	private static final int FRAMEWIDTH = 375;
	private static final int FRAMEHEIGHT = 450;

	private FoodManager am;

	private JTextField pvdField, nameField, delField, feeField;
	private JTextArea descField;

	public FoodRegister(Account loginAccount, FoodManager manager) {

		am = manager;

		// Initialize Frame
		setTitle(FRAMETITLE);
		setSize(FRAMEWIDTH, FRAMEHEIGHT);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		// Layout
		JPanel panel = new JPanel();

		GridBagConstraints gbc = new GridBagConstraints();

		// General panel layout & attribute
		panel.setLayout(new GridBagLayout());
		panel.setBorder(new EmptyBorder(30, 30, 30, 30));

		gbc.weightx = 1.0;
		gbc.gridwidth = 2;

		// Setting layout of login
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weighty = 0.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.ipady = 0;

		pvdField = new JTextField();
		nameField = new JTextField();
		descField = new JTextArea(3, 0);
		delField = new JTextField();
		feeField = new JTextField();

		if (loginAccount.getAccountType() == AccountType.SP) {
			pvdField.setText(Integer.toString(loginAccount.getID()));
			pvdField.setEditable(false);
		}

		String[] fieldNames = { "Provider ID", "Name", "Description", "Delivery Time (mins)", "Fee (HKD)" };
		JComponent[] fields = { pvdField, nameField, new JScrollPane(descField), delField, feeField };
		panel.add(new TextPanel("Add Food", fieldNames, fields), gbc);

		// Horizontal layout of buttons
		gbc.gridwidth = 1;
		gbc.weighty = 1.0;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.SOUTH;

		gbc.gridx = 0;
		gbc.gridy = 1;

		JButton registerButton = new JButton("Add");
		registerButton.addActionListener((event) -> registerFood());
		panel.add(registerButton, gbc);

		gbc.gridx = 1;

		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener((event) -> dispose());
		panel.add(cancelButton, gbc);

		add(panel);

		setVisible(true);
	}

	private void registerFood() {

		int id = am.getNextAviliableID();

		String[] foodDetails = { Integer.toString(id), pvdField.getText(), nameField.getText(), descField.getText(),
				delField.getText(), feeField.getText() };

		try {
			Food newFood = new Food(foodDetails);
			am.addRecord(newFood);

			JOptionPane.showMessageDialog(null,
					"Success!\nSuccessfully added food " + newFood.getName() + ".\nFood ID: " + newFood.getID(),
					"Success!", JOptionPane.INFORMATION_MESSAGE);

			dispose();
		} catch (InvalidDataException e) {
			JOptionPane.showMessageDialog(null,
					"Invalid Food!\nPlease enter the data according to the format.\nError: " + e.getMessage(),
					"Invalid Food!", JOptionPane.WARNING_MESSAGE);
			return;
		}

	}

}
