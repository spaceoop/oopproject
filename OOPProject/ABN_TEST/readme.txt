Abnormal Food Data,
Record 1. "20000m,100003,Curry Chicken,Chicken with curry,20,58.20", invalid Food ID "20000m", the data type of ID is Integer, String input can cause error
Record 2. "200001,Spicy Beef,Beef with hot chili,20,68.20", missing Provider ID, data absent can cause error in reading
Record 3. "200002,,Hamburger,Bread with meat,15,32.10" contains empty Provider ID, causing error
Record 4. "200004,100004,Japanese Ramen,Ramen with BBQ pork,25,$56.40", invalid Fee with "$", incorrect data type
Record 5. "200005,100003,Lunch Set,Seafood, Fried Rice,25,64.20", mismatch ProviderID & Food ID, cannot be detected, need further improvement in database integration

Abnormal Order Data,
Record 1. "300000W,100003,100001,200000,58.20,27-11-2017 15:31,00-00-0000 00:00,00-00-0000 00:00,Requested", invalid Order ID, String cannot be input in Integer field
Record 2. "300001,100003,100001,200001,68.20,27-11-2017 15:33,27-11-2017 15:37", missing Delivery Status, error in reading
Record 3. "300002,100003,100002,200001,68.20,14:42,28-11-2017 14:45,28-11-2017 15:42,Delivered", invalid Date format
Record 4. "300003,100004,100001,200002,$32.10,28-11-2017 15:04,28-11-2017 15:24,28-11-2017 15:54,Completed", invalid Fee with "$", invalid data type
Record 5. "100004,100001,200002,32.10,28-11-2017 15:05,28-11-2017 15:26,28-11-2017 15:56,Completed", missing Order ID
Record 6. "300005,100003,100000,200002,32.10,28-11-2017 30:25,28-11-2017 15:36,28-11-2017 16:26,Completed", mismatch Food ID & Provider ID, cannot be detected
Record 7. "300006,100003,100000,200004,68.20,28-11-2017 15:33,28-11-2017 15:37,00-00-0000 00:00,Delivering", invalid Food ID, not exist, cannot be detected
Record 8. "300007,100003,100002,200001,68.20,29-11-2017 14:42,00-00-0000 00:00,00-00-0000 00:00,Delivered", mismatch Delivery Status & Delivery Time, cannot be detected
Record 9. "300008,100003,100010,200000,58.20,29-11-2017 14:42,00-00-0000 00:00,00-00-0000 00:00,Payment Accepted", invalid Customer ID, not exist, cannot be detected