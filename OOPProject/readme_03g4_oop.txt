OOP Group Project
Note: Some files may use unix line ending (LF) instead of windows line ending (CRLF), please use a viewer/editor which supports unix line ending (e.g Notepad++) if you are viewing on Windows. Thank you.
----------------------------------------------------------------------------------------------------------------------------
Group Info.:
   Member List:
     Li Kwan To, Tommy		(SID:20027290)
     Au Chun Man, Andy		(SID:20027759)
     Chan Chung Yin, Barry	(SID:20029161)
     Lau Ho Man, Tommy		(SID:20027108)
   Project Name: OOP Food Delivery System
   Course Name: Object-Oriented Programming
   Course ID: CCIT4023
   Class ID: CL03
   Year: 2017-2018 Semester 1
----------------------------------------------------------------------------------------------------------------------------
Objective:
  -Provide a platform for consumers to order the food via a centralized system
  -Provide a platform for service providers to promote their meals
  -Develop a centralized service recording system to allow providers and consumers to maintain a history of service records
----------------------------------------------------------------------------------------------------------------------------
Usage:
  In the folder 03g4_oop, run 03g4_r.bat. The script will try to search for 03g4_oop.jar file and run using it.
  If it is not present, the script will tries to run with the .class files in bin folder instead.
----------------------------------------------------------------------------------------------------------------------------
Compilation:
  In the folder 03g4_oop, run with 03g4_c.bat.
  The script will first cleanup the whole bin directory and recompile all the class files.
  In addition to the .class file, a .jar file is created in the project root directory and can be run directly.
----------------------------------------------------------------------------------------------------------------------------
File Listing:
03g4_oop
�x  03g4_c.bat
�x	The compile script of the system, run to compile the system.
�x  03g4_r.bat
�x	The running script of the system, run to start the system.
�x  readme_03g4_oop.txt
�x	This readme file, contains the important info about the system.
�x  03g4_report.pdf
�x	The report file for the project.
�x  03g4_slides.pdf
�x	The presentation slides for the project.
�x  03g4_finalsubform.pdf
�x	The final submission form for the project.
�u�wABN_TEST
�x	Contain the abnormal testing files.
�u�wassets
�x	Contain images resources that is used in the program.
�u�wbin
�x	Contain compiled .class files.
�u�wdata
�x	Contain the default testing files, also used for running the program.
�u�wN_Test
�x	Contain the normal testing files.
�u�wsrc
�x	Contain the .java source code of the program.

----------------------------------------------------------------------------------------------------------------------------
Known Errors:
  - Data inconsistency:
    Required to updated common field one by one, e.g. updating the Provider ID in Food table would not update the Order table
----------------------------------------------------------------------------------------------------------------------------
